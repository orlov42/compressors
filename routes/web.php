<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;

//Регистрация
Route::post('/auth/register', 'Auth\RegisterController@register')->name("auth_register");
Route::post('/auth/login', 'Auth\LoginController@login')->name("auth_login");
Route::get('/auth/logout', 'Auth\LoginController@logout')->name("auth_logout");
Route::post('/auth/forgot', 'Auth\ForgotPasswordController@forgot')->name("auth_forgot");
Route::post('/auth/reset', 'Auth\ResetPasswordController@reset')->name("auth_reset");
Route::get('/testing', function () {
//    Mail::
});
Route::get('/testing_token/{user_email}', function ($user_email) {
    $reset = \App\Models\PasswordReset::where("user_email",$user_email)->first();
    return new \Illuminate\Http\JsonResponse($reset);
});

//--------------
Route::get('/auth/forgot/{token}',function($token){ return view('auth.recover',[ 'token' => $token ]); })->name("password_reset_form");
//--------------


//Route::get('/mail', function () {
//
//    Mail::raw('Текст письма', function($message)
//    {
//        $message->from('python.auto@ya.ru', 'Laravel');
//
//        $message->to('megateleporter777@gmail.com');
//    });
//    return view('mainpage');
//})->name("main");


Route::get('/', function () {

    //пример кэширования каждые 10 минут
//    return Cache::remember('welcome', 10, function () {
//        return view('welcome')->render();
//    });
    return view('mainpage');
})->name("main");

//TODO: добавить middleware на все маршруты
Route::get('cabinet',function(){
   return view('cabinet.ku');
})->name("cabinet-ku");

Route::get('cabinet/ustavki',function(){
    return view('cabinet.ustavki');
})->name("cabinet-ustavki");

Route::get('cabinet/regulations',function(){
    return view('cabinet.regulations');
})->name("cabinet-regulations");

Route::get('cabinet/events',function(){
    return view('cabinet.events');
})->name("cabinet-events");
Route::get('cabinet/repairs',function(){
    return view('cabinet.repairs');
})->name("cabinet-repairs");

Route::get('cabinet/settings',function(){
    return view('cabinet.settings');
})->name("cabinet-settings");

Route::get('cabinet/details/{ku}',function($ku){
    return view('cabinet.ku_details', compact('ku'));
})->where(['ku'=>'[0-9]+'])->name("ku-details");

#Rus
Route::resource('object_model', 'ObjectModelController');
Route::resource('model_parameters', 'ModelParameterController');
Route::get('marks', 'DictionaryController@getMarks');
Route::get('models', 'DictionaryController@getModelsBySymbols');
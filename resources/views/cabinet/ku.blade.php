{{--Первая вкладка личного кабинета, наследует основной шаблон личного кабинета--}}
@extends('cabinet/layout')

@section('cabinet-content')
    {{--<div id="app">--}}
    {{----}}

    {{--</div>--}}


    <div id="app">

        {{--Модальное окно для добавления КУ--}}
        <div class="ui tiny modal" data-modal="add_ku">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Добавление нового КУ</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Эксплуатирующая организация</label>
                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid selection" tabindex="0">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите организацию</div>
                                    <div class="menu" tabindex="-1">
                                        <div class="item active selected" data-value="1">Первая</div>
                                        <div class="item" data-value="2">Вторая</div>
                                        <div class="item" data-value="3">Третья</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Серийный номер устройства</label>
                            </div>
                            <div class="column">
                                <div class="ui input fluid">
                                    <input name="serial_number" placeholder='KU-123456789'>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Модель</label>
                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid search selection">
                                    <input type="hidden" name="model">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите модель</div>
                                    <div class="menu">
                                        @php
                                            for($i = 0;$i<5;$i++){
                                                $id = rand(100,999);
                                                echo "<div class='item' data-value='$id'>$id</div>";
                                            }
                                        @endphp
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Дата выпуска</label>
                            </div>
                            <div class="column">
                                <div class="ui calendar" data-type="date">
                                    <div class="ui left icon fluid input">
                                        <input name="editDate" readonly="" placeholder="01.01.2018" type="text"
                                               style="cursor: pointer;">
                                        <i class="calendar icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Адрес в сети (Unit ID)</label>
                            </div>
                            <div class="column">
                                <div class="ui input fluid">
                                    <input name="serial_number" placeholder="0" value="0">
                                </div>
                            </div>
                        </div>


                        <div class="two column row">
                            <div class="column">
                                <label>Обслуживающая организация</label>
                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid selection" tabindex="0">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите организацию</div>
                                    <div class="menu" tabindex="-1">
                                        <div class="item active selected" data-value="1">Первая</div>
                                        <div class="item" data-value="2">Вторая</div>
                                        <div class="item" data-value="3">Третья</div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Прибор</label>
                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid search selection device" tabindex="0">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите прибор</div>
                                    <div class="menu" tabindex="-1">
                                        <div class="item" data-value="0">Без прибора</div>
                                        <div class="item" data-value="1">Прибор1</div>
                                        <div class="item" data-value="2">Прибор2</div>
                                        <div class="item" data-value="3">Прибор3</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row ku-address hidden">
                            <div class="column">
                                <label>Адрес устройства</label>
                            </div>
                            <div class="column">
                                <div class="ui input fluid">
                                    <input name="ku-address">
                                </div>
                            </div>
                        </div>
                        <div class="ten wide column centered">
                            <button type="button" class="main_button">Добавить</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        {{--Модальное окно для копирования КУ--}}
        <div class="ui tiny modal" data-modal="copy_ku">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Копировать КУ</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Компрессорная установка</label>
                            </div>
                            <div class="column">
                                {{--Выпадающий список для отображения всех моделей--}}
                                {{--При выборе элемента меняется активная для копирования модель --}}

                                <semantic-dropdown
                                        v-model="choosenModelForCopy"
                                        :items="KU_LIST"
                                        default-text="Выберите модель"
                                        :class="{'fluid':true,'search':true}"
                                />

                                {{--Если выбрана модель то показать её параметры как в окне подробностей--}}

                            </div>
                        </div>

                        {{--Блок с информацией показывается только если выбрана модель--}}
                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''"
                        >
                            <div class="column"><label>Серийный номер</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.id">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''">
                            <div class="column"><label>Обслуживающая организация</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.serviceProvider.name">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''">
                            <div class="column"><label>Эксплуатирующая организация</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.operatingProvider.name">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''">
                            <div class="column"><label>Модель</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.model.name">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''">
                            <div class="column"><label>Дата выпуска</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.date">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== ''">
                            <div class="column"><label>Номер передающего устройства</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="choosenModelForCopy.deviceNumber">
                                </div>
                            </div>
                        </div>

                        <div class="two column row"
                             v-if="choosenModelForCopy !== '' && choosenModelForCopy.deviceId !== ''">
                            <div class="column"><label>Прибор</label></div>
                            <div class="column">
                                <div class="ui input fluid disabled">
                                    <input type="text"
                                           :value="'Модем #'+choosenModelForCopy.deviceId">
                                </div>
                            </div>
                        </div>
                        {{--/**/--}}

                        <div class="ten wide column centered">
                            <button type="button" class="main_button">Скопировать</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        {{--Модальное окно подтверждения удаления выбранных КУ--}}
        <div class="ui mini modal" id="modalDeleteKU" data-modal="confirm">
            <i class="close icon"></i>
            <div class="header">
                Удаление устройств
            </div>
            <div class="content">
                <p>Вы уверены что хотите удалить это устройство?</p>
                <ul class="ui list" id="KUDeleteList">
                    <li>@{{ activeKu.id }}</li>
                </ul>

            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="main_button" @click.prevent="removeKu(activeKu)">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>
        </div>

        {{--Модальное окно для редактирования--}}
        <div class="ui tiny modal" data-modal="edit">
            <i class="close icon"></i>
            <div class="content">
                <form class="ui form" id="formEditKu">
                    <div class="ui grid middle aligned">
                        <div class="sixteen wide column header">
                            <h3 class="ui header">
                                Редактирование КУ @{{ activeKu.id }}
                            </h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Серийный номер</label>
                            </div>
                            <div class="column">
                                <div class="ui fluid disabled input">
                                    <input type="text"
                                           :value="activeKu.id"
                                    >
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Обслуживающая организация</label>
                            </div>
                            <div class="column field">
                                <div id="serviceOrgDropdown" class="ui selection dropdown search fluid">
                                    <input type="hidden" name="serviceProvider">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите организацию</div>
                                    <div class="menu">
                                        <div class="item" data-value="1">
                                            ОО №1
                                        </div>
                                        <div class="item" data-value="2">
                                            ОО №2
                                        </div>
                                        <div class="item" data-value="3">
                                            ОО №3
                                        </div>
                                        <div class="item" data-value="4">
                                            ОО №4
                                        </div>
                                        <div class="item" data-value="5">
                                            ОО №5
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Эксплуатирующая организация</label>
                            </div>
                            <div class="column field">

                                <div id="operatingOrgDropdown" class="ui selection dropdown search fluid">
                                    <input type="hidden" name="operatingProvider">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите организацию</div>
                                    <div class="menu">
                                        <div class="item" data-value="1">
                                            ЭО №1
                                        </div>
                                        <div class="item" data-value="2">
                                            ЭО №2
                                        </div>
                                        <div class="item" data-value="3">
                                            ЭО №3
                                        </div>
                                        <div class="item" data-value="4">
                                            ЭО №4
                                        </div>
                                        <div class="item" data-value="5">
                                            ЭО №5
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Модель</label>
                            </div>
                            <div class="column field">
                                <div id="modelDropdown" class="ui selection dropdown search fluid">
                                    <input type="hidden" name="model">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите модель устройства</div>

                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Дата выпуска</label>
                            </div>
                            <div class="column field">
                                <div class="ui calendar" data-type="date">
                                    <div class="ui left icon fluid input">
                                        <input name="editDate" readonly=""
                                               :value="activeKu.date" type="text"
                                               style="cursor: pointer;">
                                        <i class="calendar icon"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Номер передающего устройства</label>
                            </div>
                            <div class="column field">
                                <div class="ui fluid input">
                                    <input type="text" name="deviceNumber"
                                           :value="activeKu.deviceNumber"
                                    >
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Прибор</label>
                            </div>
                            <div class="column field">
                                <div id="editDeviceIdDropdown" class="ui selection dropdown search fluid">
                                    <input type="hidden" name="deviceId">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите прибор</div>

                                </div>
                            </div>
                        </div>

                        <div class="six wide column">
                            <button type="button" class="save main_button">Сохранить</button>

                        </div>
                        <div class="six wide column right floated">
                            <button type="button" @click.prevent="initRemoveKu" class="remove main_button">
                                Удалить
                            </button>
                        </div>

                    </div>
                </form>
            </div>
        </div>

        {{--Модальное окно с подробной инфой об организации--}}
        <div class="ui tiny modal" data-modal="org">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <div class="ui grid">
                    <div class="sixteen wide column header">
                        <h3 class="ui header">
                            Подробнее об организации @{{ activeOrg.name }}
                        </h3>
                    </div>
                    <div class="two column row">
                        <div class="column"><label>ИНН</label></div>
                        <div class="column">@{{ activeOrg.inn }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>КПП</label></div>
                        <div class="column">@{{ activeOrg.kpp }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Название</label></div>
                        <div class="column">@{{ activeOrg.name }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Банковские реквизиты</label></div>
                        <div class="column">@{{ activeOrg.bank }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Юридический адрес</label></div>
                        <div class="column">@{{ activeOrg.urAddress }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Фактический адрес</label></div>
                        <div class="column">@{{ activeOrg.factAddress }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Почтовый адрес</label></div>
                        <div class="column">@{{ activeOrg.postAddress }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Email</label></div>
                        <div class="column">@{{ activeOrg.email }}</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Телефон</label></div>
                        <div class="column">@{{ activeOrg.phone }}</div>
                    </div>

                </div>
            </div>
        </div>

        {{--Модальное окно для отображения информации о приборах--}}
        {{--Просмотр/редактирование/привязка--}}
        <div class="ui tiny modal" data-modal="show_device">
            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Приборы</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="device-choose">Просмотреть данные прибора</label>

                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid selection" name="device-choose" tabindex="0">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Выберите прибор</div>
                                    <div class="menu" tabindex="-1">
                                        <div class="item" data-value="">+ Добавить прибор</div>
                                        <div class="item" data-value="1">ID: 1</div>
                                        <div class="item" data-value="2">ID: 2</div>
                                        <div class="item" data-value="3">ID: 3</div>
                                    </div>
                                </div>

                            </div>

                        </div>

                        {{--<div class="two column row">--}}
                        {{--<div class="column">--}}
                        {{--<label for="device-ID">Идентификатор</label>--}}
                        {{--</div>--}}
                        {{--<div class="column field">--}}
                        {{--<div class="ui input fluid">--}}
                        {{--<input id="device-ID" name="deviceID">--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        {{--</div>--}}
                        <div class="two column row">
                            <div class="column">
                                <label for="device-name">Название прибора</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input id="device-name" name="device-name">
                                </div>
                            </div>
                        </div>

                        {{--<div class="two column row">--}}
                            {{--<div class="column">--}}
                                {{--<label for="device-name">Частота опроса</label>--}}
                            {{--</div>--}}
                            {{--<div class="column field">--}}
                                {{--<div class="ui input fluid right labeled">--}}
                                    {{--<input id="polling_time" name="polling_time">--}}
                                    {{--<label for="polling_time" class="ui label">сек.</label>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        <div class="row two column">
                            <div class="column">
                                <label for="timezone">Часовой пояс прибора</label>
                            </div>
                            <div class="column">
                                <select class="timezone fluid" name="timezone" id="timezone">
                                    <option value="-12.0"> (GMT -12:00) Эниветок</option>
                                    <option value="-11.0"> (GMT -11:00) Остров Мидуэй, Самоа</option>
                                    <option value="-10.0"> (GMT -10:00) Гавайи</option>
                                    <option value="-9.0"> (GMT -9:00) Аляска</option>
                                    <option value="-8.0"> (GMT -8:00) Тихоокеанское время</option>
                                    <option value="-7.0"> (GMT -7:00) Горное время (США и Канада)</option>
                                    <option value="-6.0"> (GMT -6:00) Мехико</option>
                                    <option value="-5.0"> (GMT -5:00) Восточное время (США и Канада)</option>
                                    <option value="-4.0"> (GMT -4:00) Атлантическое время (Канада)</option>
                                    <option value="-3.5"> (GMT -3:30) Ньюфаундленд</option>
                                    <option value="-3.0"> (GMT -3:00) Бразилия, Буэнос-Айрес</option>
                                    <option value="-2.0"> (GMT -2:00) Срединно-Атлантическое</option>
                                    <option value="-1.0"> (GMT -1:00 час) Азорские острова</option>
                                    <option value="0.0"> (GMT) Лондон, Лиссабон, Касабланка</option>
                                    <option value="1.0"> (GMT +1:00 час) Брюссель, Мадрид, Париж</option>
                                    <option value="2.0"> (GMT +2:00) Киев, Калининград, Южная Африка</option>
                                    <option value="3.0"> (GMT +3:00) Москва, Санкт-Петербург</option>
                                    <option value="3.5"> (GMT +3:30) Тегеран</option>
                                    <option value="4.0"> (GMT +4:00) Абу-Даби, Мускат, Баку, Тбилиси</option>
                                    <option value="4.5"> (GMT +4:30) Кабул</option>
                                    <option value="5.0"> (GMT +5:00) Екатеринбург, Исламабад</option>
                                    <option value="5.5"> (GMT +5:30) Бомбей, Калькутта</option>
                                    <option value="5.75"> (GMT +5:45) Катманду</option>
                                    <option value="6.0"> (GMT +6:00) Алматы, Дакке, Коломбо</option>
                                    <option value="7.0"> (GMT +7:00) Бангкок, Ханой, Джакарта</option>
                                    <option value="8.0"> (GMT +8:00) Пекин, Перт, Сингапур, Гонконг</option>
                                    <option value="9.0"> (GMT +9:00) Токио, Сеул, Якутск</option>
                                    <option value="9.5"> (GMT +9:30) Аделаида, Дарвин</option>
                                    <option value="10.0"> (GMT +10:00) Восточная Австралия, Гуам, Владивосток</option>
                                    <option value="11.0"> (GMT +11:00) Магадан, Новая Каледония</option>
                                    <option value="12.0"> (GMT +12:00) Фиджи, Камчатка</option>
                                </select>
                            </div>
                        </div>


                        <div class="two column row">
                            <div class="column">
                                <label>Оперативный период опроса</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid right labeled">
                                    <input id="operate_registr" name="operate_registr">
                                    <label for="operate_registr" class="ui label">сек.</label>
                                </div>
                            </div>
                        </div>
                        {{--<div class="two column row">--}}
                            {{--<div class="column">--}}
                                {{--<label>Управляющий регистр</label>--}}
                            {{--</div>--}}
                            {{--<div class="column">--}}
                                {{--<div class="ui input fluid field">--}}
                                    {{--<input name="control_registr">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        <div class="two column row">
                            <div class="column">
                                <label>Конфигурационный период опроса</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid right labeled">
                                    <input id="config_registr" name="config_registr">
                                    <label for="config_registr" class="ui label">сек.</label>
                                </div>
                            </div>

                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="device-type">Тип прибора</label>
                            </div>
                            <div class="column">

                                <select class="fluid" name="device-type" id="device-type">
                                    <option value="">Выберите тип устройства</option>
                                    <option value="1">GPRS-модем</option>
                                </select>


                            </div>
                        </div>
                        <div class="two column imei row hidden">
                            <div class="column">
                                <label for="device-imei">IMEI модема</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input id="device-imei" name="device-imei">
                                </div>
                            </div>
                        </div>
                    </div>

                </form>
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="ok main_button">Сохранить</button>
                    </div>
                    <div class="column">
                        <button class="clear main_button">Очистить</button>
                        <button class="disable main_button">Отвязать</button>
                    </div>
                </div>
            </div>

        </div>

        {{--Модальное окно подтверждения удаления выбранных КУ--}}
        <div class="ui mini modal" data-modal="confirmDeleteDevice">
            <i class="close icon"></i>
            <div class="header">
                Отвязка прибора
            </div>
            <div class="content">
                <p>Вы уверены что хотите отвязать прибор от компрессорной установки</p>

            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="ok main_button">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>

        </div>

        <div class="ui tiny modal" id="chooseActiveParam">
            <i class="close icon"></i>
            <div class="content" style="padding: 1em;">
                <div class="ui grid middle aligned">
                    <div class="column sixteen wide header">
                        <h3 class="ui header">Выбор активного параметра</h3>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Параметр</label>
                        </div>
                        <div class="column">
                            <div class="ui dropdown fluid selection" name="activeParamChoose" tabindex="0">
                                <i class="dropdown icon"></i>
                                <div class="default text">Выберите параметр</div>
                                <div class="menu" tabindex="-1">
                                    <div class="item" data-value="1">Параметр 1</div>
                                    <div class="item" data-value="2">Параметр 2</div>
                                    <div class="item" data-value="3">Параметр 3</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="main_button">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>
        </div>

        <div class="ui grid" id="main_table">
            <div class="two wide column">
                <button class="main_button" data-modal="show_device">Приборы</button>

            </div>

            <div class="right floated three wide column">
                <button class="main_button" @click="initChooseParam">Выбрать активный параметр</button>
            </div>
            <div class="three wide column">
                <button class="main_button" @click="initCopyKu">Копировать КУ</button>
                {{--<button class="main_button" data-modal="copy_ku">Копировать КУ</button>--}}
            </div>

            <div class="three wide column">
                <button class="main_button" data-modal="add_ku">Добавить КУ</button>
            </div>

            <div class="row">
                <div class="sixteen wide column centered">
                    <div class="ui fluid card" id="content-card">
                        <div class="content">
                            <div class="header">Компрессорные устройства</div>
                        </div>
                        <div class="content">
                            <div class="ui grid">
                                <div class="eight wide column">
                                    <form class="ui form">
                                        <div class="inline field">
                                            <label>Отображать по</label>

                                            <new-drop
                                                    v-model="pageSize"
                                                    :items="paginators"
                                                    default-text="..."
                                                    :class="{'compact':true}"
                                            />

                                            {{--<div class="ui dropdown compact selection" tabindex="0">--}}
                                                {{--<input type="hidden" name="showCounter">--}}
                                                {{--<i class="dropdown icon"></i>--}}
                                                {{--<div class="text">10</div>--}}
                                                {{--<div class="menu" tabindex="-1">--}}
                                                    {{--<div class="item active selected" data-value="10">10</div>--}}
                                                    {{--<div class="item" data-value="50">50</div>--}}
                                                    {{--<div class="item" data-value="100">100</div>--}}
                                                {{--</div>--}}
                                            {{--</div>--}}
                                        </div>
                                    </form>

                                </div>
                                <div class="five wide column"></div>

                                <div class="sixteen wide column" style="padding-top: 0;">
                                    <div class="ui segment fluid basic" style="padding:0;">
                                        <table class="ui celled table striped center aligned selectable sortable very basic"
                                               v-if="updated"
                                               id="table-ku"
                                        >
                                            <thead>

                                            <th class="two wide" @click.prevent="sortTable('id')">
                                                <i class="filter link icon" data-name="Серийный номер" is-active="false"
                                                   data-filter="common"></i>
                                                Серийный номер
                                            </th>
                                            <th class="two wide">
                                                <i class="filter link icon" data-name="Наработка" is-active="false"
                                                   data-filter="status"></i>
                                                Наработка
                                            </th>
                                            <th class="three wide">
                                                <i class="filter link icon" data-name="Обслуживающая организация"
                                                   is-active="false" data-filter="common"></i>
                                                <span data-tooltip="Обслуживающая организация">Обслуживающая</span>
                                            </th>
                                            <th class="three wide">
                                                <i class="filter link icon" data-name="Эксплуатирующая организация"
                                                   is-active="false" data-filter="common"></i>
                                                <span data-tooltip="Эксплуатирующая организация">Эксплуатирующая</span>
                                            </th>
                                            <th class="two wide">
                                                <i class="filter link icon" data-name="Прибор"
                                                   is-active="false" data-filter="common"></i>Прибор
                                            </th>
                                            <th class="two wide" empty></th>
                                            <th class="one wide" empty></th>
                                            </thead>
                                            <tbody>

                                            <tr v-for="(ku, graphIndex) in sortedKU" :data-ku="ku.id">
                                                <td> @{{ ku.id }}</td>
                                                <td style="max-height: 200px;">
                                                    <p style="display: none;">@{{ ku.workPercent }}</p>
                                                    <div class="chart-holder">
                                                        <doughnut-chart
                                                                :counter="ku.workPercent"
                                                        >
                                                        </doughnut-chart>
                                                    </div>


                                                </td>
                                                <td>
                                                    <a
                                                            :data-organization="ku.serviceProvider.id"
                                                            @click="showOrgDetails(ku.serviceProvider)">
                                                        @{{ ku.serviceProvider.name }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a
                                                            :data-organization="ku.operatingProvider.id"
                                                            @click="showOrgDetails(ku.operatingProvider)">
                                                        @{{ ku.operatingProvider.name }}
                                                    </a>
                                                </td>
                                                <td>
                                                    <a data-action="showDevice"
                                                       :data-device="ku.deviceId"
                                                       @click="showDevice(ku.deviceId)"
                                                       v-if="ku.deviceId !== ''"
                                                       style="text-decoration: underline">Модем #@{{ ku.deviceId }}</a>
                                                    <span v-else="ku.deviceId === ''">Прибор не привязан</span>
                                                </td>

                                                <td>
                                                    <a
                                                            :href="currentUrl+'/details/'+ku.id"
                                                            class="small main_button">
                                                        Подробнее
                                                    </a>
                                                </td>
                                                <td>
                                                    <i class="link icon pencil big" style="color:#444444"
                                                       @click="editKuInformation(ku)"></i>

                                                </td>
                                            </tr>

                                            </tbody>
                                            <tfoot>
                                            <tr>
                                                <th colspan="3">
                                                    <p class="left floated">Отображено c @{{ showFrom }} по @{{showTo}}
                                                        из @{{  KU_LIST.length }}</p>
                                                </th>
                                                <th style="border-left:0;" colspan="4">
                                                    <pagination
                                                            :current-page="currentPage"
                                                            :isShow="true"
                                                            :total="total"
                                                            @change="changePage"
                                                            :page-size="pagecount"
                                                            :per-page="pageSize"
                                                            :floated="true"
                                                    />
                                                </th>
                                            </tr>
                                            </tfoot>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>

@endsection
@section('cabinet-scripts')

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>

    <script src="{{asset('/js/cabinet/ku.js')}}"></script>
    <script src="{{asset('/js/cabinet/charts.min.js')}}"></script>
    <script src="{{asset('/js/moment.min.js')}}"></script>

    <script src="https://unpkg.com/vue-chartjs@3.0.1-rc2/dist/vue-chartjs.js"></script>
    <script src="{{asset('/js/vue/ku.js')}}"></script>

    <script src="{{asset('/js/datatables.min.js')}}"></script>

    <script>
        $(document).ready(function () {
            // $('#table-ku').dataTable()
        })
    </script>
@endsection

@section('cabinet-styles')
    <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">

@endsection

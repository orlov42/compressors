@extends('cabinet/layout')
@section('cabinet-content')

    <div id="app">
        {{--Модальное окно с подробной инфой об организации--}}
        <div class="ui tiny modal" data-modal="org">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <div class="ui grid">
                    <div class="sixteen wide column header">
                        <h3 class="ui header">
                            Подробнее об организации **ORG_NAME**
                        </h3>
                    </div>
                    <div class="two column row">
                        <div class="column"><label>ИНН</label></div>
                        <div class="column">54654654545646</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>КПП</label></div>
                        <div class="column">54654654545646</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Название</label></div>
                        <div class="column">Название организации</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Банковские реквизиты</label></div>
                        <div class="column">Реквизиты</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Юридический адрес</label></div>
                        <div class="column">Адрес</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Фактический адрес</label></div>
                        <div class="column">Адрес</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Почтовый адрес</label></div>
                        <div class="column">Адрес</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Email</label></div>
                        <div class="column">Адрес</div>
                    </div>

                    <div class="two column row">
                        <div class="column"><label>Телефон</label></div>
                        <div class="column">Телефон</div>
                    </div>

                </div>
            </div>
        </div>

        {{--Модальное окно для добавления заявки на КУ--}}
        <div class="ui tiny modal" data-modal="addOrder">
            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Добавить заявку на КУ</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Компрессорная установка:</label>
                            </div>
                            <div class="column">

                                <select class="ui dropdown search selection fluid" name="" id="">
                                    <option value="">Выберите модель</option>
                                    <option value="1">1123331</option>
                                    <option value="2">3324551</option>
                                    <option value="3">44324234</option>
                                </select>

                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Комментарий (необязательно)</label>
                            </div>
                            <div class="column">
                                <div class="ui input fluid">
                                    <input type="text">
                                </div>
                            </div>
                        </div>


                        <div class="ten wide column centered">
                            <button type="button" class="main_button">Создать заявку</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        {{--Модальное окно для заявки на группу КУ--}}
        <div class="ui tiny modal" data-modal="addManyOrders">
            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Добавить заявку на группу КУ</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Компрессорные установки:</label>
                            </div>
                            <div class="column">

                                <select class="ui dropdown search selection fluid multiple" name="" id="">
                                    <option value="">Выберите модели</option>
                                    <option value="1">1123331</option>
                                    <option value="2">3324551</option>
                                    <option value="3">44324234</option>
                                </select>

                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Комментарий (необязательно)</label>
                            </div>
                            <div class="column">
                                <div class="ui input fluid">
                                    <input type="text">
                                </div>
                            </div>
                        </div>


                        <div class="ten wide column centered">
                            <button type="button" class="main_button">Создать заявки</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>


        <div class="ui grid">

            <div class="right floated three wide column">
                <button class="main_button" @click="showAddManyOrders">Добавить заявку на группу КУ</button>
            </div>
            <div class="three wide column">
                <button class="main_button" @click="showAddRepairOrder">Добавить заявку на КУ</button>
            </div>

            <div class="sixteen wide column centered">
                <div class="ui fluid card" id="content-card">
                    <div class="content">
                        <div class="header">Заявки на ремонт</div>
                    </div>
                    <div class="content">
                        <div class="ui grid">
                            <div class="sixteen wide column" style="padding-top: 0;">
                                <table id="repair-table"
                                       class="ui celled table striped center aligned selectable very basic sortable">
                                    <thead>

                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Эксплуатирующая организация" is-active="false" data-filter="common"></i>
                                        <span data-tooltip="Эксплуатирующая организация" data-position="top center">
                                        ЭО
                                    </span>
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Обслуживающая организация" is-active="false" data-filter="common"></i>
                                        <span data-name="Обслуживающая организация" data-tooltip="Обслуживающая организация" data-position="top center">
                                        ОО
                                    </span>
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Дата создания" is-active="false" data-filter="date"></i>
                                        Создано
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Дата выполнения" is-active="false" data-filter="date"></i>
                                        Исполнено
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Этап работы" is-active="false" data-filter="common"></i>
                                        Этап работы
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="КУ" is-active="false" data-filter="common"></i>
                                        КУ
                                    </th>
                                    <th class="two wide">
                                        <i class="filter link icon" data-name="Статус" is-active="false" data-filter="common"></i>
                                        Статус
                                    </th>
                                    <th class="one wide" empty>
                                        История заявки
                                    </th>
                                    </thead>
                                    <tbody>
                                    @for ($i = 1; $i < 6; $i++)
                                        <tr data-ku="{{$i}}">
                                            <td><a data-organization="{{$i+15}}" href="javascript:void(0)">Организация</a></td>
                                            <td> <a data-organization="{{$i+15}}" href="javascript:void(0)">Организация</a></td>
                                            <td>05.12.2017</td>
                                            <td>05.12.2017</td>
                                            <td>Этап {{$i}}</td>
                                            <td>@php echo rand() @endphp</td>
                                            <td>
                                                <div class="ui inline dropdown">
                                                    <div class="default text">Статус заявки</div>
                                                    <i class="dropdown icon"></i>
                                                    <div class="menu">
                                                        <div class="item" data-value="status1">Статус 1</div>
                                                        <div class="item" data-value="status2">Статус 2</div>
                                                        <div class="item" data-value="status3">Статус 3</div>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><i class="icon big talk link"></i></td>
                                        </tr>
                                    @endfor
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="4">
                                            <text class="left floated">Отображено 5 из 35</text></th>
                                        <th style="border-left:0;" colspan="4">
                                            <div class="ui right floated pagination menu">
                                                <a class="icon item">
                                                    <i class="left chevron icon"></i>
                                                </a>
                                                <a class="active item">1</a>
                                                <a class="item">2</a>
                                                <a class="item">...</a>
                                                <a class="item">4</a>
                                                <a class="item">5</a>
                                                <a class="icon item">
                                                    <i class="right chevron icon"></i>
                                                </a>
                                            </div>
                                        </th>
                                    </tr>
                                    </tfoot>
                                </table>

                            </div>

                        </div>
                    </div>
                </div>
            </div>


            <div style="display:none;" id="event_details" class="row">
                <div class="six wide column centered">
                    <div class="ui fluid card" id="content-card">
                        <div class="content">
                            <div class="header">Подробнее о событии</div>
                        </div>
                        <div class="content">
                            <table class="ui table very basic celled selectable striped event-details">
                                <tbody>
                                <tr>
                                    <td class="eight wide">Наименование события</td>
                                    <td>Наименование</td>
                                </tr>
                                <tr>
                                    <td>Модель</td>
                                    <td>C3PO R2D2</td>
                                </tr>
                                <tr>
                                    <td>Обслуживающая организация</td>
                                    <td>Название</td>
                                </tr>
                                <tr>
                                    <td>Эксплуатирующая организация</td>
                                    <td>Название</td>
                                </tr>


                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="fourteen wide column centered">
                <ol class="comments-list">
                    <li>

                    </li>
                </ol>
            </div>


        </div>
    </div>


@endsection

@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/repairs.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{asset('/js/vue/repairs.js')}}"></script>

@endsection

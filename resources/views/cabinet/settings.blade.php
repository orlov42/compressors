@extends('cabinet/layout')
@section('cabinet-content')

    <div id="app">
        {{--FILTERS--}}

        <div class="ui mini modal" id="filter1" data-filter="filter">
            <input type="hidden" :value="currentFilterField">
            <i class="close icon"></i>
            <div class="content">
                <form class="ui form">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Фильтрация по полю @{{ filterFieldName }}</h3>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Операция фильтра</label>
                            </div>
                            <div class="column">
                                <div class="ui dropdown fluid selection" tabindex="0">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Фильтр</div>
                                    <div class="menu" tabindex="-1">
                                        <div class="item"
                                             v-for="filter in filterOperations"
                                             :data-value="filter.value"
                                        >
                                            @{{ filter.text }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label>Значение</label>
                            </div>
                            <div class="column">
                                <div class="ui input">
                                    <input name="value" :value="currentFilterValue">
                                </div>
                            </div>
                        </div>

                        <div class="sixteen wide column">
                            <button type="button" class="main_button" @click.prevent="confirmFilter(currentFilterField)">Подтвердить</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        {{--FILTERS END--}}


        {{--Модальное окно добавления новой модели--}}
        <div class="ui tiny modal" data-modal="add_model">
            <i class="close icon"></i>
            <div class="column header">
                Добавление новой модели
            </div>
            <div class="content">
                <form class="ui form" id="addModelForm">
                    <input type="hidden" name="model_work_register" value="0">
                    {{csrf_field()}}
                    <div class="ui grid middle aligned">
                        <div class="two column row">
                            <div class="column">
                                <label for="mark_id">Выберите марку</label>
                            </div>
                            <div class="column field">

                                <div class="ui selection fluid dropdown mark_id">
                                    <input name="mark_id" type="hidden" value="">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Марка</div>
                                    <div class="menu"></div>
                                </div>

                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="model_name">Название модели</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input type="text" name="model_name">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="main_button" @click.prevent="addModel">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>

        </div>

        {{--Модальное окно добавления нового параметра--}}
        <div class="ui tiny modal" data-modal="add_param">
            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form" id="addParamForm">
                    {{csrf_field()}}
                    <input type="hidden" name="model_id" :value="currentModel">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Добавление нового параметра</h3>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_name">Наименование параметра</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input placeholder="Название" type="text" name="parameter_name" id="parameter_name">
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_address">Адрес регистра</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input placeholder="Адрес" type="text" name="parameter_address"
                                           id="parameter_address">
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_format">Формат хранения</label>
                            </div>
                            <div class="column field">

                                <select class="ui dropdown selection fluid" name="parameter_format"
                                        id="parameter_format">
                                    <option value="">Выберите формат хранения</option>
                                    <option value="1">String</option>
                                    <option value="2">Float</option>
                                </select>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_read_func">Функция записи</label>
                            </div>
                            <div class="column field">
                                <select class="ui dropdown selection fluid" name="parameter_read_func"
                                        id="parameter_read_func">
                                    <option value="">Выберите функцию записи</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                </select>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                {{--TODO: заменить prameter_write--}}
                                <label for="prameter_write_func">Функция чтения</label>
                            </div>
                            <div class="column field">
                                <select class="ui dropdown selection fluid" name="prameter_write_func"
                                        id="prameter_write_func">
                                    <option value="">Выберите функцию чтения</option>
                                    <option value="01">01</option>
                                    <option value="02">02</option>
                                </select>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Параметр наработки</label>
                            </div>
                            <div class="column">
                                <div class="ui checkbox">
                                    <input name="example" type="checkbox">
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label>Аварийный параметр</label>
                            </div>
                            <div class="column">
                                <div class="ui checkbox">
                                    <input name="example2" type="checkbox">
                                </div>
                            </div>
                        </div>


                        <div class="ten wide column centered">
                            <button type="button" class="main_button" @click="addParam">Добавить</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        {{--Модальное окно редактирования выбранного параметра--}}
        <div class="ui tiny modal" data-modal="edit_param">

            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form" id="editParamForm">
                    <input type="hidden" name="parameter_id" value="">
                    <input type="hidden" name="parameter_control" value="0">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Редактирование параметра</h3>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_name">Наименование параметра</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input type="text" name="parameter_name" id="parameter_name">
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_address">Адрес регистра</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input type="text" name="parameter_address" id="parameter_address">
                                </div>
                            </div>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_format">Формат хранения</label>
                            </div>
                            <div class="column field">
                                <select class="ui dropdown selection fluid" name="parameter_format"
                                        id="parameter_format">
                                    <option value="">Выберите формат хранения</option>
                                    <option value="1">String</option>
                                    <option value="2">Float</option>
                                </select>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="prameter_write_func">Функция записи</label>
                            </div>
                            <div class="column field">
                                <select class="ui dropdown selection fluid" name="prameter_write_func"
                                        id="prameter_write_func">
                                    <option value="">Выберите функцию записи</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                </select>
                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="parameter_read_func">Функция чтения</label>
                            </div>
                            <div class="column field">
                                <select class="ui dropdown selection fluid" name="parameter_read_func"
                                        id="parameter_read_func">
                                    <option value="">Выберите функцию чтения</option>
                                    <option value="1">01</option>
                                    <option value="2">02</option>
                                </select>
                            </div>
                        </div>

                        <div class="row two column">
                            <div class="column">
                                <button type="button" class="main_button" @click="editParam">Сохранить</button>
                            </div>
                            <div class="column">
                                <button type="button" class="cancel main_button">Отменить</button>

                            </div>
                        </div>

                    </div>

                </form>

            </div>
        </div>

        {{--Модальное окно подтверждения удаления параметра--}}
        <div class="ui tiny modal" data-modal="confirm_delete">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui grid middle aligned">
                    <div class="sixteen wide column header">
                        <h3 class="ui header">
                            Удаление параметра
                        </h3>
                    </div>
                    <div class="row">
                        <div class="column">
                            <div class="ui icon negative message">
                                <i class="exclamation icon"></i>
                                <p style="margin: 0;">Вы уверены что хотите удалить параметр @{{ paramForDelete_name }}?</p>
                            </div>
                        </div>
                    </div>
                </div>

                {{--<input type="hidden" name="currentParam" :value="paramForDelete">--}}
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button class="main_button" @click.prevent="deleteParam">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>

        </div>

        {{--Модальное окно для подтверждения удаления модели--}}
        <div class="ui tiny modal" data-modal="confirm_delete_model">
            <i class="close icon"></i>

            <div class="content">
                <div class="ui grid middle aligned">
                    <div class="sixteen wide column header">
                        <h3 class="ui header">
                            Удаление модели
                        </h3>
                    </div>
                    <div class="row">
                        <div class="column">
                            <div class="ui icon negative message">
                                <i class="exclamation icon"></i>
                                <p style="margin: 0;">Вы уверены что хотите удалить модель @{{ currentModelName }}?</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button @click.prevent="deleteModel" class="ok main_button">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>

        </div>

        {{--Модальное окно редактирования модели--}}
        <div class="ui tiny modal" data-modal="edit_Model">
            <i class="close icon"></i>
            <div class="header">
                Редактирование модели "@{{ currentModelName }}"
            </div>
            <div class="content">
                <form class="ui form" id="editModelForm">
                    {{--<input type="hidden" name="model_work_register" value="0">--}}
                    {{csrf_field()}}
                    <div class="ui grid middle aligned">
                        <div class="two column row">
                            <div class="column">
                                <label for="">Выберите марку</label>
                            </div>
                            <div class="column field">
                                <div class="ui selection fluid dropdown mark_id">
                                    <input name="mark_id" type="hidden" :value="currentMark">
                                    <i class="dropdown icon"></i>
                                    <div class="default text">Марка</div>
                                    <div class="menu"></div>
                                </div>

                            </div>
                        </div>
                        <div class="two column row">
                            <div class="column">
                                <label for="model_name">Название модели</label>
                            </div>
                            <div class="column field">
                                <div class="ui input fluid">
                                    <input type="text"
                                           name="model_name"
                                           :value="currentModelName">
                                </div>
                            </div>
                        </div>

                    </div>
                </form>
            </div>
            <div class="actions">
                <div class="ui grid two column">
                    <div class="column">
                        <button @click.prevent="editModel" class="main_button">Подтвердить</button>
                    </div>
                    <div class="column">
                        <button class="cancel main_button">Отменить</button>
                    </div>
                </div>
            </div>
        </div>

        {{--Модальное окно для копирования параметров другого устройства--}}
        <div class="ui tiny modal" data-modal="copy_params">
            <i class="close icon"></i>
            <div class="content">
                <div class="ui inverted dimmer">
                    <div class="ui text loader">Загрузка</div>
                </div>
                <form class="ui form" id="copyParamsForm">
                    <div class="ui grid middle aligned">
                        <div class="column sixteen wide header">
                            <h3 class="ui header">Копирование параметров другого устройства</h3>
                        </div>

                        <div class="two column row">
                            <div class="column">
                                <label for="original_model">Устройство для копирования</label>
                            </div>
                            <div class="column field">
                                <div class="ui dropdown search selection fluid id_original">
                                    <input type="hidden" name="id_copy">
                                    <div class="default text">Выберите модель</div>
                                    <i class="dropdown icon"></i>
                                </div>
                            </div>
                            {{csrf_field()}}
                            {{--<input type="hidden" name="id_copy" :value="currentModel">--}}
                            <input type="hidden" name="action" value="copy">
                        </div>

                        <div class="sixteen wide centered column">
                            <div class="ui negative message">
                                <p>
                                    Данная операция удалит все параметры у "<span id="currentModel">@{{ currentModelName }}</span>"
                                    и заменит
                                    их
                                    параметрами
                                    устройства для копирования
                                </p>
                            </div>
                        </div>


                        <div class="eight wide column centered">
                            <button type="button" @click.prevent="copyParams" class="main_button">Скопировать</button>
                        </div>
                    </div>

                </form>

            </div>
        </div>

        <div class="ui grid">
            <div class="fourteen wide column centered">


                <div class="ui search selection fluid dropdown" id="find_model">
                    <input type="hidden" :value="currentModel" @change="changeModel">
                    <div class="default text">Выберите модель</div>
                    <i class="dropdown icon"></i>
                    <div class="menu">
                        {{--<div class="item"--}}
                        {{--v-for="model in allModels"--}}
                        {{--:data-value="model.value"--}}
                        {{--:data-text="model.text"--}}
                        {{-->--}}
                        {{--@{{ model.name }}--}}
                        {{--</div>--}}
                    </div>
                </div>
            </div>
            <div class="fourteen wide column centered">
                <div class="ui fluid card" id="content-card">
                    <div class="content">
                        <div class="header">Таблица параметров</div>
                    </div>

                    <div class="content">

                        <div class="ui message" v-show="(currentModel==='' || currentModel === '-1')"
                             id="warning_message">
                            <div class="header">
                                Не выбрана модель
                            </div>
                            <p>Для просмотра параметров необходимо выбрать модель из выпадающего списка сверху</p>
                        </div>

                        <div class="row" v-show="(currentModel!=='' && currentModel!=='-1')"
                             style="display:none;    margin: 0 0 30px 0;">
                            <div class="eight wide column">
                                <form class="ui form">
                                    <div class="inline field">
                                        <label>Отображать по</label>

                                        <dropdown
                                                v-model="params_limit"
                                                :items="paginators"
                                                default-text="..."
                                                :class="{'compact':true}"
                                                @input="updateParams"
                                        />

                                    </div>
                                </form>

                            </div>
                        </div>


                        <div id="main_table" v-show="(currentModel!=='' && currentModel!=='-1')" style="display:none"
                             class="ui grid">
                            <div class="sixteen wide column" v-if="updated" style="padding-top: 0;">
                                <table :data-sort-dir="sort_dir" :data-sort-field="sort_field" id="all_params"
                                       class="ui celled table striped center aligned selectable very basic sortable">
                                    <thead>
                                    <th class="three wide" @click="sort('parameter_name')">
                                        <i class="filter link icon"
                                           :class="{'notactive':hasFilter('parameter_name')}"
                                           @click.prevent="toggle_filter('parameter_name')"
                                        ></i>
                                        Название параметра</th>
                                    <th class="three wide" @click="sort('parameter_address')">
                                        <i class="filter link icon"
                                           :class="{'notactive':hasFilter('parameter_address')}"
                                           @click.prevent="toggle_filter('parameter_address')"
                                        ></i>
                                        Адрес регистра
                                    </th>
                                    <th class="two wide" @click="sort('parameter_format')">
                                        <i class="filter link icon"
                                           :class="{'notactive':hasFilter('parameter_format')}"
                                           @click.prevent="toggle_filter('parameter_format')"
                                        ></i>
                                        Формат хранения</th>
                                    {{--TODO: prameter--}}
                                    <th class="one wide" @click="sort('prameter_write_func')">
                                        <i class="filter link icon"
                                           :class="{'notactive':hasFilter('prameter_write_func')}"
                                           @click.prevent="toggle_filter('prameter_write_func')"
                                        ></i>
                                        Функция записи</th>
                                    <th class="one wide" @click="sort('parameter_read_func')">
                                        <i class="filter link icon"
                                           :class="{'notactive':hasFilter('parameter_read_func')}"
                                           @click.prevent="toggle_filter('parameter_read_func')"
                                        ></i>
                                        Функция чтения</th>
                                    <th class="one wide" empty></th>
                                    <th class="one wide" empty></th>
                                    </thead>
                                    <tbody>
                                    <tr v-if="currentModel!=='' && currentModel !== '-1'" v-for="param in params">
                                        <td>@{{ param.parameter_name }}</td>
                                        <td>@{{ param.parameter_address }}</td>
                                        <td>@{{ param.parameter_format }}</td>
                                        {{--TODO: prameter--}}
                                        <td>@{{ param.prameter_write_func }}</td>
                                        <td>@{{ param.parameter_read_func }}</td>
                                        <td><i class='link icon pencil big' style='color:#444444'
                                               @click="showEditParam(param.parameter_id)"></i></td>
                                        <td><i class='link icon remove big' style='color:#444444'
                                               @click="showDeleteParam(param.parameter_id)"></i></td>
                                    </tr>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <th colspan="7"><p class="left floated">Отображено c @{{ showedFrom }} по @{{
                                                showedTo }}
                                                из @{{ params_total }}</p>
                                            <div class="ui pagination menu right floated" v-if="paginateisShow">
                                                <a class="item" v-if="paginateisShowEdge"
                                                   :class="{disabled: currentPage === 1}"
                                                   @click.prevent="changePage(1)">
                                                    «
                                                </a>
                                                <a class="item" v-for="page in pages"
                                                   :class="{active: page === currentPage}"
                                                   @click.prevent="changePage(page)">
                                                    @{{page}}
                                                </a>
                                                <a class="item" v-if="paginateisShowEdge"
                                                   :class="{disabled: currentPage === totalPages}"
                                                   @click.prevent="changePage(totalPages)">
                                                    »
                                                </a>
                                            </div>
                                        </th>

                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="ui grid" id="action_buttons" style="display:none"
                     v-show="(currentModel!=='' && currentModel!=='-1')">

                    <div class="row four column right floated">

                        <div class="column">
                            <button class="main_button" @click="showEdit">Редактировать модель</button>
                        </div>

                        <div class="column">
                            <button class="main_button" @click="showAddParam">Добавить параметр</button>
                        </div>
                        <div class="column">
                            <button class="main_button"
                                    {{--data-modal="copy_params"--}}
                                    @click="showCopyModal"
                            >Скопировать параметры
                            </button>
                        </div>
                        <div class="column">
                            <button @click.prevent="confirmModelDelete" class="cancel main_button"><i
                                        class="trash icon"></i>Удалить модель
                            </button>
                        </div>
                    </div>


                </div>

            </div>

        </div>

    </div>


@endsection
@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/settings.js')}}"></script>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="{{asset('/js/vue/settings.js')}}"></script>

@endsection
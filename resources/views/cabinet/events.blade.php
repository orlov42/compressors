@extends('cabinet/layout')
@section('cabinet-content')
    <div class="ui grid">

        <div class="sixteen wide column centered" id="example1">
            <div class="ui fluid card" id="content-card">
                <div class="content">
                    <div class="header">Таблица событий</div>
                </div>
                <div class="content">
                    <div class="ui grid">
                        <div class="eight wide column">
                            <div class="inline field"><label>Отображать по</label>
                                <div class="ui dropdown compact selection" tabindex="0">
                                    <input type="hidden" name="showCounter">
                                    <i class="dropdown icon"></i>
                                    <div class="text">100</div>
                                    <div class="menu" tabindex="-1">
                                        <div data-value="100" class="item active selected">100</div>
                                        <div data-value="500" class="item">500</div>
                                        <div data-value="1000" class="item">1000</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sixteen wide column" style="padding-top: 0;">
                            <table id="event-table"
                                   class="ui celled table striped center aligned selectable very basic sortable">
                                <thead>
                                <th class="one wide" empty>#</th>
                                <th class="three wide">
                                    <i class="filter link icon" data-name="Тип события" is-active="false"
                                       data-filter="common"></i>
                                    Тип события
                                </th>
                                <th class="three wide">
                                    <i class="filter link icon" data-name="Серийный номер КУ" is-active="false"
                                       data-filter="common"></i>
                                    Серийный номер КУ
                                </th>
                                <th class="three wide">
                                    <i class="filter link icon" data-name="Дата и время" is-active="false"
                                       data-filter="date"></i>
                                    Дата и время
                                </th>
                                <th class="three wide" empty></th>
                                </thead>
                                <tbody>
                                @for ($i = 1; $i < 6; $i++)
                                    <tr>
                                        <td>{{$i}}</td>
                                        <td>Важное событие {{$i}}</td>
                                        <td>@php echo rand(); @endphp</td>
                                        <td>05.12.2017</td>
                                        <td><button class="small main_button">Подробнее</button></td>
                                    </tr>
                                @endfor
                                </tbody>
                                <tfoot>
                                <th colspan="5">
                                    <div class="ui right floated pagination menu">
                                        <a class="icon item">
                                            <i class="left chevron icon"></i>
                                        </a>

                                        <a class="icon item">
                                            <i class="right chevron icon"></i>
                                        </a>
                                    </div>
                                </th>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row" style="display: none" id="event_details">
            <div class="fourteen wide column centered">
                <ul class="timeline">
                    <li class="clearfix">
                        <time class="tl-time">
                            <h3 class="text">23:15</h3>
                            <p>05th September 2013</p>
                        </time>
                        <i class="circular inverted talk icon"
                           style="background-color: rgb(42, 184, 119) !important;"></i>
                        <div class="tl-content">
                            <div class="panel">
                                <div class="panel-heading">Events</div>
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. Nam fermentum, massa sit amet pulvinar laoreet, sem mi viverra
                                    turpis, ac porta felis nisl eu odio.
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="clearfix">
                        <time class="tl-time">
                            <h3 class="text">23:15</h3>
                            <p>05th September 2013</p>
                        </time>
                        <i class="circular inverted talk icon"
                           style="background-color: rgb(42, 184, 119) !important;"></i>
                        <div class="tl-content">
                            <div class="panel">
                                <div class="panel-heading">Events</div>
                                <div class="panel-body">
                                    Текст комментария, текст комментария, текст комментария, текст комментария, текст
                                    комментария, текст комментария, текст комментария, текст комментария, текст
                                    комментария, текст комментария, текст комментария, текст комментария, текст
                                    комментария, текст комментария, текст комментария, текст комментария, текст
                                    комментария, текст комментария, текст комментария, текст комментария, текст
                                    комментария
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="clearfix">
                        <time class="tl-time">
                            <h3 class="text">23:15</h3>
                            <p>05th September 2013</p>
                        </time>
                        <i class="circular inverted talk icon"
                           style="background-color: rgb(42, 184, 119) !important;"></i>
                        <div class="tl-content">
                            <div class="panel">
                                <div class="panel-heading">Events</div>
                                <div class="panel-body">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam laoreet tellus ut
                                    tincidunt euismod. Nam fermentum, massa sit amet pulvinar laoreet, sem mi viverra
                                    turpis, ac porta felis nisl eu odio.
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="fourteen wide column centered">
                <form class="ui form">
                    <div class="field">
                        <textarea rows="3" placeholder="Введите ваш комментарий"></textarea>
                    </div>
                </form>
            </div>
            <div class="three wide column centered" style="margin-top: 20px;">
                <button class="main_button">Отправить</button>
            </div>
        </div>



    </div>

@endsection
@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/events.js')}}"></script>
@endsection
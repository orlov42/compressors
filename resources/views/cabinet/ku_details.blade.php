@extends('cabinet/layout')
@section('cabinet-content')

    <div class="ui grid" style="" id="ku_details">
        <div class="row">
            <div class="column">
                {{--TODO: вставить ссылку на текущего пользователя--}}
                <a class="main_link" href="{{ route('cabinet-ku', 1)}}">
                    <i class="left long arrow icon"></i>
                    Назад
                </a>
            </div>
        </div>

        {{--Колонка с меню и заголовком--}}
        <div class="sixteen wide column" style="padding-bottom: 0">
            <div class="ui fluid segment">
                <div class="ui grid middle aligned">
                    <div class="eight wide column">
                        <h2 class="ui header left floated">
                            Устройство номер {{$ku}}
                        </h2>
                    </div>
                    <div class="six wide column right aligned right floated">
                        <i class="big link sync icon"></i>
                        <i class="big link check icon" style="display: none"></i>
                    </div>
                </div>
                <div class="ui grid" style="margin-top:0">
                    <div class="sixteen wide column">
                        <div class="ui secondary menu">
                            <a class="item active" data-tab="params">Параметры</a>
                            <a class="item" data-tab="table">Таблицы</a>

                            <a class="item" data-tab="graphs">Графики</a>
                            <a class="item" data-tab="writeparams">Запись параметров</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="sixteen wide column">

                {{--Вкладка для отображения последних данных о параметрах--}}
                <div class="ui fluid active tab segment" data-tab="params">
                    <table class="ui celled table striped selectable sortable">
                        <thead>
                        <th class="eight wide">Параметр</th>
                        <th class="four wide">Код параметра</th>
                        <th class="three wide">Значение</th>
                        </thead>
                        <tbody>
                        <tr>
                            <td>Параметр 1</td>
                            <td>inputRead21</td>
                            <td>37.7</td>
                        </tr>
                        <tr>
                            <td>Параметр 2</td>
                            <td>inputRead31</td>
                            <td>37.7</td>
                        </tr>
                        <tr>
                            <td>Параметр 3</td>
                            <td>inputRead41</td>
                            <td>37.7</td>
                        </tr>
                        <tr>
                            <td>Параметр 4</td>
                            <td>inputRead51</td>
                            <td>37.7</td>
                        </tr>
                        </tbody>
                    </table>
                </div>

                {{--Вкладка для отображения информации по выбранным параметрам--}}
                {{--за выбранный период--}}
                <div class="ui fluid tab segment" data-tab="table">
                    {{--Форма для применения фильтра по датам--}}
                    <form class="ui form">
                        <div class="ui grid">
                            <div class="two wide column field">
                                <div class="ui calendar" id="rangestart">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Введите дату">
                                    </div>
                                </div>
                            </div>
                            <div class="two wide column field">
                                <div class="ui calendar" id="rangeend">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Введите дату">
                                    </div>
                                </div>
                            </div>
                            <div class="four wide column field">
                                {{--Отображение параметров--}}
                                <div class="ui dropdown multiple search selection fluid" id="dropdownChooseParams">
                                    <div class="default text">Параметры для отображения</div>
                                    <i class="dropdown icon"></i>
                                </div>
                            </div>
                            <div class="two wide column">
                                <button class="ui button fluid" data-style="default" data-action="accept_filter">
                                    Применить
                                </button>
                            </div>


                        </div>
                    </form>
                    <table class="ui celled center aligned table striped selectable sortable" id="detailsMainTable">
                        <thead>
                        <tr class="headerRow">
                            <th class="one wide">#</th>
                            <th class="two wide">Дата время</th>
                            <th class="three wide">Параметр 2</th>
                            <th class="three wide">Параметр 3</th>
                        </tr>


                        </thead>
                        <tbody>

                        <tr>
                            <td>1</td>
                            <td>02-05-2018 00:01:07</td>
                            <td>37.02</td>
                            <td>33.02</td>
                        </tr>
                        <tr>
                            <td>2</td>
                            <td>02-05-2018 00:01:07</td>
                            <td>33.02</td>
                            <td>33.02</td>
                        </tr>
                        <tr>
                            <td>3</td>
                            <td>02-05-2018 00:01:07</td>
                            <td>37.02543</td>
                            <td>37.027</td>
                        </tr>

                        </tbody>
                    </table>

                </div>

                {{--Вкладка для отображения графиков за определенный период--}}
                <div class="ui fluid tab segment" data-tab="graphs">

                    <form class="ui form">
                        <div class="ui grid">
                            <div class="two wide column field">
                                <div class="ui calendar rangestart">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Введите дату" value="5.1.2018 0:00">
                                    </div>
                                </div>
                            </div>
                            <div class="two wide column field">
                                <div class="ui calendar rangeend">
                                    <div class="ui input left icon">
                                        <i class="calendar icon"></i>
                                        <input type="text" placeholder="Введите дату"  value="5.9.2018 0:00">
                                    </div>
                                </div>
                            </div>
                            <div class="four wide column field">
                                {{--Отображение параметров--}}
                                <div class="ui dropdown multiple search selection choose_params"
                                     id="dropdownChooseParams">
                                    <div class="default text">Параметры для отображения</div>
                                    <i class="dropdown icon"></i>
                                </div>
                            </div>
                            <div class="two wide column">
                                <button class="ui button fluid" data-style="default" data-action="accept_filter_graphs">
                                    Применить
                                </button>
                            </div>


                        </div>
                    </form>

                    <h2 class="ui header center aligned">
                        График с
                        <span class="dateFirst">
                            01.05.2018 00:00
                        </span>по
                        <span class="dateSecond">
                            09.05.2018 00:00

                        </span>
                    </h2>
                    <div class="ui grid">
                        <div class="nine wide centered column">
                            <canvas id="lineChart"></canvas>

                        </div>
                    </div>



                </div>

                <div class="ui fluid tab segment" data-tab="writeparams">Запись параметров
                </div>
            </div>
        </div>
    </div>


@endsection
@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/charts.min.js')}}"></script>
    <script src="{{asset('/js/cabinet/ku_details.js')}}"></script>

    <script src="{{asset('/js/moment.min.js')}}"></script>
    <script>
        //если выбран параметр (по месяцам)
        let months = ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"]

        let makeARandomNumber = function () {
            return Math.floor(Math.random() * 9);
        }
        let data1 = Array(12).fill(0).map(makeARandomNumber);
        let data2 = Array(12).fill(0).map(makeARandomNumber);

        //если выбран параметр по дням

        function generateLabel(option) {
            let labels = [];
            switch (option) {
                case 'week':
                    //получили 10 недель до текущей
                    for (let i = 10; i > 0; i--) {
                        labels.push(moment().subtract(i, 'week').format('w'))
                    }
                    break;

            }
        }

        generateLabel('week');
        let data = {
            labels: months,
            datasets: [
                {
                    label: 'Параметр 1',
                    data: data1,
                    fill: 'false',
                    borderColor: 'rgba(164, 155, 196,.8)',

                },
                {
                    label: 'Параметр 2',
                    data: data2,
                    fill: 'false',
                    borderColor: 'rgba(255, 102, 111,.8)',

                }
            ]
        };

        let chartOptions = {
            legend: {
                display: false,
            },
            tooltips: {
                mode: 'point',
                intersect: true
            },
            hover: {
                mode: 'point',
                intersect: true
            }
        };
        let lineChart = new Chart('lineChart', {
            type: 'line',
            data: data,
            options: chartOptions,

        });

    </script>





@endsection


@extends('cabinet/layout')

@section('cabinet-content')

    <div class="ui mini modal" data-modal="TO-hours">
        <i class="close icon"></i>
        <div class="content">
            <div class="ui grid middle aligned">
                <div class="sixteen wide column header">
                    <h3 class="ui header">Добавление ТО</h3>
                </div>
                <div class="two column row">
                    <div class="column">
                        <label>Количество часов:</label>
                    </div>
                    <div class="column">
                        <div class="ui fluid input right labeled">
                            <input name="TO-hours" data-mask="00000000" placeholder="100">
                            <label class="ui label">ч.</label>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="actions">
            <div class="ui grid centered">
                <div class="eight wide center aligned column">
                    <button class="small main_button">
                        Подтвердить
                    </button>
                </div>
            </div>

        </div>
    </div>
    <div class="ui tiny modal" data-modal="workname">
        <i class="close icon"></i>
        <div class="content">

            <form class="ui form">
                <div class="ui grid middle aligned">
                    <div class="column sixteen wide header">
                        <h3 class="ui header">Добавление новой работы</h3>
                    </div>
                    <div class="row">
                        <div class="column five wide">
                            <label>Название работы:</label>
                        </div>
                        <div class="eleven wide column field">
                            <div class="ui fluid input">
                                <input name="TO-workname" placeholder="Название">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="five wide column">
                            <label>Этапы работы:</label>
                        </div>
                        <div class="eleven wide column field">
                            <textarea name="work-comment"
                                      rows="3"
                                      placeholder="Введите этапы через точку с запятой"
                            ></textarea>
                        </div>
                    </div>
                </div>
                {{--<div class="field">--}}
                    {{--<div class="ui fluid input">--}}
                        {{--<input name="TO-workname" placeholder="Введите название работы">--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="field">--}}
                    {{--<textarea name="work-comment" rows="3"--}}
                              {{--placeholder="Введите этапы через точку с запятой"></textarea>--}}
                {{--</div>--}}
            </form>

        </div>
        <div class="actions">
            <div class="ui grid centered">
                <div class="eight wide center aligned column">
                    <button class="small main_button">
                        Подтвердить
                    </button>
                </div>
            </div>

        </div>
    </div>



    <div class="ui mini modal" data-modal="edit_work">
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form">
                <div class="field">
                    <textarea name="edit_work-comment" rows="3"
                              placeholder="Введите этапы через точку с запятой"></textarea>
                </div>
            </form>

        </div>
        <div class="actions">
            <div class="ui grid centered">
                <div class="eight wide center aligned column">
                    <button class="small main_button">
                        Подтвердить
                    </button>
                </div>
            </div>

        </div>
    </div>



    <div class="ui grid">
        <div class="fourteen wide column centered">
            <div class="ui search selection dropdown fluid" id="find_model">
                <div class="default text">Выберите модель</div>
                <i class="dropdown icon"></i>
            </div>
        </div>
        <div class="fourteen wide column centered">
            <div class="ui fluid card" id="content-card">
                <div class="content">
                    <div class="header">Конструктор регламентов</div>
                </div>

                <div class="content">
                    <div class="ui message" id="warning_message">
                        <div class="header">
                            Не выбрано устройство
                        </div>
                        <p>Для работы с конструктором необходимо выбрать модель устройства из выпадающего списка
                            сверху</p>
                    </div>
                    <form class="ui form" id="main_content" style="display:none">

                        <div class="ui grid">

                            <div class="sixteen wide column" id="context1">
                                <div class="ui tabular menu" id="main_TO_menu">
                                    <a class="item active" data-tab="to1">
                                        ТО #1 (10 часов)
                                        <div class="ui red basic circular floating label">
                                            x
                                        </div>
                                    </a>

                                    <div class="right menu">
                                        <a class="item" data-action="create-new-TO">
                                            <i class="link add icon"></i>
                                            Добавить ТО
                                        </a>
                                    </div>
                                </div>

                                <div class="ui grid tab" id="example_TO_tab">
                                    <div class="four wide column">
                                        <div class="ui vertical fluid tabular menu">
                                            <a class="item" data-action="add-work">
                                                <i class="add icon"></i>
                                                Добавить работу
                                            </a>
                                        </div>
                                    </div>
                                    <div class="twelve wide right floated stretched column">

                                    </div>
                                </div>

                                <div id="all_tabs">
                                    <div class="ui grid active tab" data-tab="to1">
                                        <div class="four wide column">
                                            <div class="ui vertical fluid tabular menu">
                                                <a class="item active" data-tab="to1-1">
                                                    Работа 1
                                                    <i class="close icon"></i>
                                                </a>
                                                <a class="item" data-action="add-work">
                                                    <i class="add icon"></i>
                                                    Добавить работу
                                                </a>
                                            </div>
                                        </div>
                                        <div class="twelve wide right floated stretched column">
                                            <div class="ui basic segment tab active" data-tab="to1-1">
                                                Здесь необходимо добавить все поля для конкретной работы 1
                                                <a class="edit_work_link">Редактировать</a>
                                            </div>
                                            <div class="ui basic segment tab" data-tab="to1-2">
                                                Здесь необходимо добавить все поля для конкретной работы 2
                                                <a class="edit_work_link">Редактировать</a>
                                            </div>
                                        </div>

                                    </div>

                                </div>


                            </div>
                        </div>
                    </form>


                </div>
            </div>

        </div>

    </div>
@endsection
@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/regulations.js')}}"></script>
@endsection
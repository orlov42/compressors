@extends('cabinet/layout')
@section('cabinet-content')
    <div class="ui grid">
        <div class="fourteen wide column centered">
            <div class="ui dropdown fluid search selection" id="find_ku">
                <input type="hidden" name="find_ku">
                <i class="dropdown icon"></i>
                <div class="default text">Выберите устройство</div>
                <div class="menu">
                    @php
                        for($i = 0;$i<5;$i++){
                            $id = rand(1,10000);
                            echo "<div class='item' data-value='$id'>$id</div>";
                        }
                    @endphp
                </div>
            </div>
        </div>
        <div class="fourteen wide column centered">
            <div class="ui fluid card" id="content-card">
                <div class="content">
                    <div class="header">Таблица уставок</div>
                </div>
                <div class="content">
                    <div class="ui message" id="warning_message">
                        <div class="header">
                            Не выбрано устройств
                        </div>
                        <p>Для просмотра уставок необходимо выбрать модель устройства из выпадающего списка сверху</p>
                    </div>
                    <div id="main_table" style="display:none" class="ui grid">
                        <div class="sixteen wide column" style="padding-top: 0;">
                            <table id="all_params" class="ui celled table striped center aligned selectable very basic sortable">
                                <thead>
                                <th class="three wide">Название параметра</th>
                                <th class="three wide">
                                    <i class="filter link icon" data-name="Адрес регистра"
                                       is-active="false" data-filter="common"></i>
                                    Адрес регистра
                                </th>

                                <th class="two wide">Формат хранения</th>
                                <th class="one wide">Функция записи</th>
                                <th class="one wide">Функция чтения</th>
                                </thead>
                                <tbody>
                                <tr><td>Название 1</td><td>R32141234</td><td>String</td><td>01</td><td>02</td></tr>
                                <tr><td>Название 2</td><td>R3323234</td><td>Float</td><td>02</td><td>012</td></tr>
                                <tr><td>Название 3</td><td>R3323234</td><td>Float</td><td>02</td><td>012</td></tr>
                                <tr><td>Название 4</td><td>R3323234</td><td>Float</td><td>02</td><td>012</td></tr>
                                <tr><td>Название 5</td><td>R3323234</td><td>Float</td><td>02</td><td>012</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
@section('cabinet-scripts')
    <script src="{{asset('/js/cabinet/ustavki.js')}}"></script>

@endsection
{{--Шаблон в целом для всего кабинета, наследует общую структуру--}}
@extends('layouts/layout')

@section('styles')
    <link rel="stylesheet" href="{{asset('css/cabinet.css')}}">
    <link rel="stylesheet" href="{{asset('semantic/components/calendar.css')}}">
    <link rel="stylesheet" href="{{asset('css/comments.css')}}">

    @if (Route::currentRouteName()=='ku-details')
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.2.1/themes/default/style.min.css"/>
    @endif
    @yield('cabinet-styles')
@endsection


@section('content')
    <div class="ui mini modal" data-filter="common">
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form">
                <div class="ui grid middle aligned">
                    <div class="column sixteen wide header">
                        <h3 class="ui header">Фильтрация по полю <span name="filter-name"></span></h3>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Операция фильтра</label>
                        </div>
                        <div class="column">
                            <div class="ui dropdown fluid selection" tabindex="0">
                                <select id="filter-number" name="filter-number">
                                    <option value="not-equal">≠</option>
                                    <option value="equal">=</option>
                                    <option value="like">~</option>
                                </select>
                                <i class="dropdown icon"></i>
                                <div class="text">5</div>
                                <div class="menu" tabindex="-1">
                                    <div class="item active selected" data-value="not-equal">≠</div>
                                    <div class="item" data-value="equal">=</div>
                                    <div class="item" data-value="like">~</div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Значение</label>
                        </div>
                        <div class="column">
                            <div class="ui input">
                                <input name="value">
                            </div>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <button type="button" class="main_button">Подтвердить</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <div class="ui mini modal" data-filter="date">
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form">
                <div class="ui grid middle aligned">
                    <div class="column sixteen wide header">
                        <h3 class="ui header">Фильтрация по полю <span name="filter-name"></span></h3>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Операция фильтра</label>
                        </div>
                        <div class="column">
                            <div class="ui dropdown fluid selection filter-date" tabindex="0">
                                <input type="hidden" name="filter-date">
                                <i class="dropdown icon"></i>
                                <div class="text"></div>
                                <div class="menu" tabindex="-1">
                                    <div class="item active selected" data-value="not-equal">≠</div>
                                    <div class="item" data-value="equal">=</div>
                                    <div class="item" data-value="between">><</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Значение</label>
                        </div>
                        <div class="column">
                            <div class="ui calendar" data-type="date">
                                <div class="ui left icon fluid input">
                                    <input id="filter-date" name="value" readonly="" type="text"
                                           style="cursor: pointer;">
                                    <i class="calendar icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="two column row additionalDate" style="display:none">
                        <div class="column">
                            <label>Значение 2</label>
                        </div>
                        <div class="column">
                            <div class="ui calendar" data-type="date">
                                <div class="ui left icon fluid input">
                                    <input id="filter-date-second" name="value" readonly="" type="text"
                                           style="cursor: pointer;">
                                    <i class="calendar icon"></i>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <button type="button" class="main_button">Подтвердить</button>
                    </div>
                </div>

            </form>

        </div>
    </div>

    <div class="ui tiny modal" data-filter="status">
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form">
                <div class="ui grid middle aligned">
                    <div class="column sixteen wide header">
                        <h3 class="ui header">Фильтрация по полю <span name="filter-name"></span></h3>
                    </div>
                    <div class="two column row">
                        <div class="column">
                            <label>Статусы для отображения</label>
                        </div>
                        <div class="column">
                            <div id="filterModelDropdown" class="ui dropdown fluid selection search multiple"
                                 tabindex="0">
                                <input type="hidden" name="value">
                                <i class="dropdown icon"></i>
                                <div class="text"></div>
                                <div class="menu" tabindex="-1">
                                    <div class="item" data-value="status1">Статус 1</div>
                                    <div class="item" data-value="status2">Статус 2</div>
                                    <div class="item" data-value="status3">Статус 3</div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="sixteen wide column">
                        <button type="button" class="main_button">Подтвердить</button>
                    </div>
                </div>

            </form>
        </div>
    </div>

    <div class="full height">
        <div class="toc">
            <div class="ui vertical large inverted menu" id="cabinet_menu">
                <div class="avatar_border">
                    {{--TODO: вставить настоящую аватарку пользователя--}}
                    <img class="ui image" src="{{asset('/img/user-default.png')}}">
                    <div class="ui dimmer">
                        <div class="content">
                            <div class="center">
                                <i class="big upload icon"></i>
                            </div>
                        </div>
                    </div>
                </div>
                <p class="text">
                    User
                </p>
                {{--TODO: вставить ссылку для авторизованного пользователя--}}

                <a href="{{route('cabinet-ku')}}"
                   class="item @if (Route::currentRouteName()=='cabinet-ku' || Route::currentRouteName()=='ku-details') active @endif">
                    <i class="large microchip icon"></i>
                    <span>Объекты</span>
                </a>

                <a href="{{route('cabinet-ustavki')}}"
                   class="item @if (Route::currentRouteName()=='cabinet-ustavki') active @endif">
                    <i class="large file outline icon"></i>
                    <span>Уставки</span>
                </a>

                <a href="{{route('cabinet-regulations')}}"
                   class="item @if (Route::currentRouteName()=='cabinet-regulations') active @endif">
                    <i class="checked calendar icon large"></i>
                    <span>Регламенты</span>
                </a>

                <a class="item @if (Route::currentRouteName()=='cabinet-events') active @endif"
                   href="{{route('cabinet-events')}}">
                    <i class="large icon warning"></i>
                    <span>События</span>
                </a>
                <a class="item @if (Route::currentRouteName()=='cabinet-repairs') active @endif"
                   href="{{route('cabinet-repairs')}}">
                    <i class="large configure icon"></i>
                    <span>Ремонт</span>

                </a>
                <a class="item @if (Route::currentRouteName()=='cabinet-settings') active @endif"
                   href="{{route('cabinet-settings')}}">
                    <i class="large icon setting"></i>
                    <span>Параметры КУ</span>
                </a>
            </div>
        </div>
        <div class="content">
            <div class="ui fluid segment" id="sgm-notify">

                <i class="big link icon sidebar"></i>
                <div class="ui compact secondary menu right floated" id="alerts">
                    <a class="item" id="messages">
                        <i class="large comments icon"></i>
                        <div class="floating ui small red label">5</div>
                    </a>
                    <div class="ui popup transition hidden" id="popup-messages" style="padding:0;">

                        <div class="ui grid centered">
                            <div class="fifteen wide column" style="margin-top: 1em;">
                                <div class="comments">
                                    <div class="ui comments">

                                        @for($i = 0; $i < 3; $i++)
                                            <div class="comment">
                                                <a class="avatar">
                                                    <img src="{{asset('/img/user-default.png')}}">
                                                </a>
                                                <div class="content">
                                                    <a class="author">Имя пользователя</a>
                                                    <div class="text">
                                                        Часть его сообщения
                                                    </div>
                                                    <div class="metadata">
                                                        <span class="date">10 минут назад</span>
                                                    </div>
                                                </div>
                                            </div>
                                            @if($i<2)
                                                <div class="ui divider"></div>
                                            @endif
                                        @endfor
                                    </div>
                                </div>
                            </div>
                            <div class="sixteen wide column" style="padding-bottom: 0;">
                                <button style="top: 4px;width: 102%;right: 2px;"
                                        class="ui fluid button bottom attached red">
                                    Все сообщения
                                </button>
                            </div>
                        </div>

                    </div>
                    <a class="item">
                        <i class="large info icon"></i>
                        <div class="floating ui small blue label">
                            5
                        </div>
                    </a>
                    <a class="item">
                        <i class="large sign out link icon"></i>
                    </a>
                </div>
            </div>
            <div class="ui basic segment" id="cabinet-content">
                @yield('cabinet-content')
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    <script type="application/javascript" src="{{asset('/js/cabinet/layout.js')}}"></script>
    <script type="application/javascript" src="{{asset('/js/mask.js')}}"></script>
    <script type="application/javascript" src="{{asset('/js/tablesort.js')}}"></script>
    <script type="application/javascript" src="{{asset('/semantic/components/calendar.js')}}"></script>
    @if (Route::currentRouteName()=='ku-details')
        <script type="text/javascript" src="https://static.jstree.com/latest/assets/dist/jstree.min.js"></script>
        <script src="https://rawgit.com/adamjimenez/jstree-table/master/jstreetable.js"></script>
    @endif

    <script>
        $(document).ready(function(){
            let data =
            $('.sign.out.icon').on('click',function(){
                $.ajax({
                    url:'/auth/logout',
                    data:{_token:'{{csrf_token()}}'},
                    success:function(response){
                        window.location.href='/'
                    }
                })
            })
        })
    </script>
    @yield('cabinet-scripts')
@endsection
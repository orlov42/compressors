@extends('layouts/layout')
@section('content')
    <div class="ui modal mini auth">
        <div class="header">
            Авторизация
        </div>
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form login">
                {{csrf_field()}}
                <div class="ui grid centered">
                    <div class="column sixteen wide">
                        <div class="field">
                                <span class="custom_input_span">
                                    <input class="basic-slide" name="user_email" id="login" type="text"
                                           placeholder="Ваш email"/>
                                    <label for="login">Email</label>
                                </span>
                        </div>
                    </div>
                    <div class="column sixteen wide">
                        <div class="field">
                                <span class="custom_input_span">
                                    <input class="basic-slide" name="user_password" id="password" type="password"
                                           placeholder="Ваш пароль"/>
                                    <label for="password">Пароль</label>
                                </span>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <div class="ui error message"></div>
                    </div>

                    <div class="row">

                        <div class="eight wide column middle aligned">
                            <span class="info">?</span>
                            <a class="recover" href="#">Забыли пароль</a>
                        </div>
                        <div class="eight wide column">
                            <button class="ui button submit-link">Войти</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="ui modal register">
        <div class="header">
            Регистрация
        </div>
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form" id="register_form">
                <div class="ui grid">
                    <div class="eight wide column">
                        <div class="field">
                            <span class="custom_input_span">
                                <select class="ui dropdown basic-slide" name="user_role" id="user_role">
                                    <option value="">Выберите роль пользователя</option>
                                    <option value="0">Производитель</option>
                                    <option value="1">Обслуживающая организация</option>
                                    <option value="2">Эксплуатирующая организация</option>
                                </select>
                                <label for="user_role">Тип компании</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_name" name="company_name"
                                       type="text" placeholder="Название вашей компании"/>
                                <label for="company_name">Компания</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_inn" name="company_inn"
                                       type="text" placeholder="Введите ИНН"/>
                                <label for="company_inn">ИНН</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_kpp" name="company_kpp"
                                       type="text" placeholder="Введите КПП"/>
                                <label for="company_kpp">КПП</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_address_ur" name="company_address_ur"
                                       type="text" placeholder="Введите юридический адрес"/>
                                <label for="company_address_ur">Юр.адрес</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_address_fact" name="company_address_fact"
                                       type="text" placeholder="Введите фактический адрес компании"/>
                                <label for="company_address_fact">Факт.адрес</label>
                            </span>
                        </div>

                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="user_password" name="user_password"
                                       type="password" placeholder="Введите пароль"/>
                                <label for="user_password">Пароль</label>
                            </span>
                        </div>

                    </div>
                    <div class="eight wide column">
                        <div class="field"> 
                            <span class="custom_input_span">
                                <input class="basic-slide" id="company_bank" name="company_bank"
                                       type="text" placeholder="Введите банковские реквизиты"/>
                                <label for="company_bank">Банк</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="post_index" name="company_index"
                                       type="text" placeholder="Введите почтовый индекс"/>
                                <label for="company_index">Индекс</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="user_name" name="user_name"
                                       type="text" placeholder="ФИО ответственного лица"/>
                                <label for="user_name">ФИО</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="user_phone" name="user_phone"
                                       type="text" placeholder="Телефон ответственного лица"/>
                                <label for="user_phone">Телефон</label>
                            </span>
                        </div>
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="user_email" name="user_email"
                                       type="text" placeholder="Email ответственного лица"/>
                                <label for="user_email">Email</label>
                            </span>
                        </div>

                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" id="user_password_confirm" name="user_password_confirm"
                                       type="password" placeholder="Введите подтверждение пароля"/>
                                <label for="user_password_confirm">Пароль</label>
                            </span>
                        </div>
                        <div class="field" style="padding-top: 6px;">
                            <div class="ui checkbox">
                                <input type="checkbox" tabindex="0" class="hidden" name="confirmation">
                                <label for="confirmation" style="color:#131313 !important;font-size:14px !important;">
                                    Я прочитал и согласен с
                                    <a href="#">условиями использования</a>
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="sixteen wide column">
                        <div class="ui error message"></div>
                    </div>

                    <div class="four wide column centered">

                        <button id="reg_btn" class="ui fluid button submit-link" style="background: #7ab893">
                            Зарегистрироваться
                        </button>
                    </div>

                </div>
                {{csrf_field()}}
            </form>

        </div>
        {{--<button class="ui positive button">--}}
        {{--Зарегистрироваться--}}
        {{--</button>--}}
    </div>

    <div class="ui mini modal recover">
        <div class="header">
            Восстановление пароля
        </div>
        <i class="close icon"></i>
        <div class="content">
            <form class="ui form" id="recover_pass">
                <div class="ui grid centered">
                    <div class="column sixteen wide">
                        <div class="field">
                            <span class="custom_input_span">
                                <input class="basic-slide" name="user_email" type="text" placeholder="Ваш email"/>
                                <label for="user_email">Email</label>
                            </span>
                        </div>
                    </div>
                    <div class="column sixteen wide">
                        <div class="ui error message"></div>
                        <div class="ui success message" style="margin: 0;">
                            <p>Сообщение с паролем отправлено на указанную почту</p>
                        </div>
                    </div>
                    <div class="column nine wide">
                        <button class="ui button submit-link">Восстановить</button>
                    </div>
                </div>
                {{csrf_field()}}
            </form>


        </div>
    </div>

    {{--Первый блок + верхнее меню--}}
    <div class="ui segment fluid section" id="first_block"
         style="background: url({{asset('/img/bg-slide1.png')}}) no-repeat;">
        <div class="ui segment fluid" id="menu_segment">
            <div class="ui container">
                <div class="ui menu">
                    <a class="logo" href="/">
                        {{--Евразкомпрессор--}}
                        <img class="ui small image" alt="Евразкомпрессор" src="{{asset('/img/evraz-logo.png')}}">
                    </a>
                    <div class="right menu" id="navigation">
                        <a href="#first_block" class="active item">Главная</a>
                        <a href="#features_container" class="item">Преимущества</a>
                        <a href="#stats_segment" class="item">Наш сервис</a>
                        <a href="#howitworks" class="item">Демонстрация</a>
                        <a href="#partners" class="item">Партнеры</a>
                        <a href="#contact_form" class="item">Контакты</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="lazy_load-1 transition hidden">
            <div class="ui container header_text">

                {{--Слайдер--}}
                <div class="ui grid centered" id="slick-slider" style="margin-top:10em;">
                    <div class="one wide column left floated middle aligned arrows">
                        <i class="huge angle left icon link" style="color:#fff"></i>
                    </div>

                    <div class="twelve wide column transition visible" data-slide="0">
                        <h1 class="ui header center aligned">Заголовок 0</h1>
                        <h2 class="ui header center aligned">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in imperdiet lorem. Proin ac
                            tempus
                            ante, sit amet.
                        </h2>
                    </div>
                    @for($i=1;$i<3;$i++)
                        <div class="twelve wide column transition hidden" data-slide="{{$i}}">
                            <h1 class="ui header center aligned">Заголовок {{$i}}</h1>
                            <h2 class="ui header center aligned">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin in imperdiet lorem. Proin
                                ac
                                tempus
                                ante, sit amet.
                            </h2>
                        </div>
                    @endfor
                    <div class="one wide column right floated middle aligned arrows">
                        <i class="huge angle right icon link" style="color:#fff"></i>
                    </div>
                </div>

                <div class="ui grid stackable centered" style="padding:2em 0 10em 0;">

                    @guest
                        <div class="four wide column right aligned">
                            {{--<button data-modal="auth" class="main_button">Вход</button>--}}
                            <button data-modal="auth" class="btn btn-4 btn-4a">Вход</button>
                        </div>
                        <div class="five wide column">
                            {{--<button data-modal="register" class="main_button">Регистрация</button>--}}
                            <button data-modal="register" class="btn btn-4 btn-4a">Регистрация</button>

                        </div>
                    @endguest
                    @auth
                            <div class="six wide column">
                                {{--<button data-modal="auth" class="main_button">Вход</button>--}}
                                <a href="/cabinet" class="btn btn-4 btn-4a" >Личный кабинет</a>
                            </div>
                    @endauth

                </div>
            </div>
        </div>
    </div>
    {{--Блок с преимуществами--}}
    <div class="ui fluid segment section" id="features_container">
        <div class="ui container">
            <h1 class="ui header center aligned">
                Преимущества
            </h1>
            <h3 class="ui header center aligned">
                Здесь вы можете разместить текст о преимуществах
            </h3>
            <div class="ui grid three column">
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="feature column transition hidden">
                    <div class="ui grid">
                        <div class="center aligned middle aligned column three wide">
                            <i class="circular inverted green setting icon"></i>
                        </div>
                        <div class="column thirteen wide">
                            <div class="ui card feature">
                                <div class="content">
                                    <div class="header">
                                        Формы работы
                                    </div>
                                    <div class="description">
                                        Быстрый и удобный онлайн интерфейс
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Блок со статистикой--}}
    <div class="ui fluid segment section"
         data-parallax="scroll"
         data-image-src="{{asset('/img/counter-back.jpg')}}" id="stats_segment">
        <div class="ui container">
            <div class="ui three column grid center aligned">
                <div class="column">
                    <div class="ui huge statistic transition hidden">
                        <div class="value" data-counter="50"></div>
                        <div class="label">
                            компрессоров
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="ui huge statistic transition hidden">
                        <div class="value" data-counter="100"></div>
                        <div class="label">дилеров</div>
                    </div>
                </div>
                <div class="column">
                    <div class="ui huge statistic transition hidden">
                        <div class="value" data-counter="450"></div>
                        <div class="label">предотвращено аварий</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--Блок "Как это работает"--}}
    <div class="ui fluid segment section" id="howitworks">
        <div class="ui container">
            <h1 class="ui header center aligned">
                Как это работает
            </h1>
            <h3 class="ui header center aligned">
                Здесь разместить текст о работе с системой
            </h3>
            <div class="ui grid">
                <div class="column five wide left floated">
                    <div class="ui vertical fluid menu">
                        <a class="item active transition hidden" data-step="1">
                            Шаг номер
                            <span class="number">1</span>
                        </a>
                        <a class="item transition hidden" data-step="2">
                            Шаг номер
                            <span class="number">2</span>
                        </a>
                        <a class="item transition hidden" data-step="3">
                            Шаг номер
                            <span class="number">3</span>
                        </a>
                        <a class="item transition hidden" data-step="4">
                            Шаг номер
                            <span class="number">4</span>
                        </a>
                        <a class="item transition hidden" data-step="5">
                            Шаг номер
                            <span class="number">5</span>
                        </a>
                        <a class="item transition hidden" data-step="6">
                            Шаг номер
                            <span class="number">6</span>
                        </a>
                    </div>
                </div>
                <div class="column eight wide right floated" id="step_segments">
                    <div class="ui fluid card transition hidden" data-step="1">
                        <div class="header">
                            Название шага 1
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                    <div class="ui fluid card transition hidden" data-step="2">
                        <div class="header">
                            Название шага 2
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                    <div class="ui fluid card transition hidden" data-step="3">
                        <div class="header">
                            Название шага 3
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                    <div class="ui fluid card transition hidden" data-step="4">
                        <div class="header">
                            Название шага 4
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                    <div class="ui fluid card transition hidden" data-step="5">
                        <div class="header">
                            Название шага 5
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                    <div class="ui fluid card transition hidden" data-step="6">
                        <div class="header">
                            Название шага 6
                        </div>
                        <div class="description">
                            Основные философские взгляды. Если вещи действительно существуют, то необходимым образом
                            существуют и идеи вещей; так что без идеи вещь не существует или сама вещь остается
                            непознаваемой.
                            <img class="ui large image centered" src="{{asset('img/test-step1.png')}}">
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    {{--Блок с партнерами--}}
    <div class="ui fluid segment section" id="partners">
        <div class="ui container" style="margin-bottom:3em">
            <h1 class="ui header center aligned" style="margin: 1em 0;">
                Партнеры
            </h1>
            <i class="icon angle left large link inverted bordered prev"></i>
            <div class="ui grid four column partners-carousel">
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner1.png')}}">
                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image link centered"
                             src="{{asset('img/partner2.png')}}">

                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner3.png')}}">
                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner4.png')}}">
                    </div>
                </div>

                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner5.png')}}">
                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image link centered"
                             src="{{asset('img/partner6.png')}}">

                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner7.png')}}">
                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner8.png')}}">
                    </div>
                </div>
                <div class="column">
                    <div class="ui fluid card">
                        <img class="ui image centered link"
                             src="{{asset('img/partner9.png')}}">
                    </div>
                </div>

            </div>
            <i class="icon angle right large link inverted bordered next"></i>
        </div>
    </div>
    {{--Блок с обратной связью--}}
    <div class="ui fluid segment section" id="contact_form"
         style="background:url({{asset('/img/contact_background.png')}}) no-repeat;">
        <div class="ui container">
            <h1 class="ui center aligned header">
                Задать вопрос
            </h1>
            <form class="ui form">
                <div class="ui grid centered">
                    <div class="column six wide transition hidden">
                        <div class="ui fluid input large field">
                            <input type="text" placeholder="Ваше имя">
                        </div>
                    </div>
                    <div class="column six wide transition hidden">
                        <div class="ui fluid input large field">
                            <input type="text" placeholder="Ваш email">
                        </div>
                    </div>
                    <div class="column twelve wide transition hidden">
                        <div class="ui fluid input large field">
                            <input type="text" placeholder="Тема вашего вопроса">
                        </div>
                    </div>
                    <div class="column twelve wide transition hidden">
                        <div class="field">
                            <textarea rows="3" placeholder="Ваш вопрос..."></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="column four wide transition hidden">
                            <button class="main_button">Отправить</button>
                        </div>
                    </div>
                </div>
            </form>

        </div>

    </div>
    {{--Черная полоска футера--}}

    {{--Карта и блок с информацией--}}

    {{--<div class="ui card" id="contact_info" style="">--}}
    {{--<div class="content">--}}
    {{--<div class="ui massive list">--}}
    {{--<div class="item">--}}
    {{--<i class="big call icon"></i>--}}
    {{--<div class="content middle aligned">--}}
    {{--<div class="header">--}}
    {{--+34234 23423423--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="item">--}}
    {{--<i class="big skype icon"></i>--}}
    {{--<div class="content middle aligned">--}}
    {{--<div class="header">--}}
    {{--skype_login--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--<div class="item">--}}
    {{--<i class="big mail icon"></i>--}}
    {{--<div class="content middle aligned" style="padding-left: 0.3em;">--}}
    {{--<div class="header">--}}
    {{--email@mail.ru--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}
    {{--</div>--}}

    {{--Контактная инф-ия--}}
    <div class="ui fluid segment" id="contact_info">
        <div class="ui container">
            <h1 class="ui header center aligned">Контактная информация</h1>
            <div class="ui grid four column">
                <div class="column">
                    <div class="ui large list">
                        <div class="item">
                            <i class="large marker icon"></i>
                            <div class="content middle aligned">
                                <div class="header">
                                    Адрес
                                </div>
                                <div class="description">
                                    454080, г. Челябинск<br>ул. Cони Кривой, д 73 оф. 303/3
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="column">
                    <div class="ui large list">
                        <div class="item">
                            <i class="large wait icon"></i>
                            <div class="content middle aligned">
                                <div class="header">
                                    Режим работы
                                </div>
                                <div class="description">
                                    Пн. – Пт.: с 9:00 до 18:00<br>
                                    Сб. - Вс.: выходные
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="ui large list">
                        <div class="item">
                            <i class="large call icon"></i>
                            <div class="content middle aligned">
                                <div class="header">
                                    Телефон
                                </div>
                                <div class="description">
                                    +7 (351) 223-16-64
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="column">
                    <div class="ui large list">
                        <div class="item">
                            <i class="large mail icon"></i>
                            <div class="content middle aligned">
                                <div class="header">
                                    Email
                                </div>
                                <div class="description">
                                    info@evrazcompressor.ru
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--<div class="five wide column">--}}
                {{--<div class="ui large list">--}}
                {{--<div class="item">--}}
                {{--<i class="large skype icon"></i>--}}
                {{--<div class="content middle aligned">--}}
                {{--<div class="header">--}}
                {{--Skype--}}
                {{--</div>--}}
                {{--<div class="description">--}}
                {{--sale@evrazcompressor.ru--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <div class="ui fluid segment" id="map"></div>
    {{--Футер--}}
    <div class="ui fluid segment" id="footer_black">
        <div class="ui container">
            <div class="ui menu">
                <a class="logo" href="/">
                    {{--Евразкомпрессор--}}
                    <img class="ui small image" alt="Евразкомпрессор" src="{{asset('/img/evraz-logo.png')}}">
                </a>

                <span class="copyright">
                    2018
                </span>

            </div>
        </div>
    </div>

@endsection
@section('styles')
    <link rel="stylesheet" href="{{asset('css/main.css')}}">
    <link rel="stylesheet" href="{{asset('css/input.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/slick-theme.css')}}">

    <link rel="stylesheet" href="{{asset('buttons/css/buttons.css')}}">
    <link rel="stylesheet" href="{{asset('buttons/css/progressbutton.css')}}">
    <style>
        #howitworks .vertical.menu .active.item {
            background: url('{{asset("/img/tab-active.svg")}}') no-repeat 0;
            color: #fff;
        }
    </style>
@endsection

@section('scripts')
    <script src="{{asset('js/slider.js')}}"></script>

    <script src="{{asset('js/mask.js')}}"></script>
    <script src="{{asset('js/parallax.min.js')}}"></script>
    <script src="{{asset('js/highlightActiveMenu.js')}}"></script>
    <script src="{{asset('buttons/js/classie.js')}}"></script>
    <script src="{{asset('buttons/js/modernizr.custom.js')}}"></script>
    <script src="{{asset('buttons/js/progressButton.js')}}"></script>

    <script src="{{asset('js/slick.min.js')}}"></script>
    <script src="{{asset('js/carousel.js')}}"></script>

    <script src="{{asset('js/main.js')}}"></script>



    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDsBI32UNhDyELcsGBB5SD82el8K7JhAHc&callback=initMap">
    </script>
@endsection


@extends('layouts/layout')
@section('content')
    <div class="ui container" id="app">
        <div class="ui segment">

            <form action="" class="ui form" :class="{loading:isLoading, error:hasError}">
                <input type="hidden" name="token" value="{{$token}}">
                <h1 class="ui dividing header">Создание нового пароля</h1>
                <div class="ui grid centered">
                    <div class="ten wide column">

                        <div class="field" :class="{error:!passwordMatch}">
                            <label for="user_password">Новый пароль</label>
                            <div class="ui input">
                                <input type="text" name="user_password" v-model="user_password">

                            </div>
                        </div>

                        <div class="field" :class="{error:!passwordMatch}">
                            <label for="confirmation">Подтверждение</label>
                            <div class="ui input">
                                <input type="text" name="confirmation" v-model="user_password_confirmation">
                            </div>
                        </div>
                    </div>
                    <div class="ten wide column" v-if="hasError">
                        <div class="ui error message">
                            <p>@{{ errorText }}</p>

                        </div>
                    </div>
                    <div class="eight wide column center aligned">
                        <button class="ui button submit-link" @click.prevent="checkAndSubmit">Подтвердить</button>
                    </div>
                    {{--<div class="sixteen wide column">--}}
                    {{--<div class="ui warning message">--}}
                    {{--<i class="close icon"></i>--}}
                    {{--<h2 class="header">Пожалуйста, введите новый пароль</h2>--}}
                    {{--</div>--}}

                    {{--</div>--}}
                </div>


            </form>


        </div>
    </div>
@endsection
@section('styles')

    <style>
        body {
            background: #eeeeee !important
        }

        .header, label {
            font-family: Roboto, sans-serif !important;
            font-weight: 400 !Important;
        }

        label {
            font-size: 16px !important;
        }

        .submit-link {
            background: #2ab877 !important;
            border-radius: 3px;
            color: #fff !important;
            padding: 13px 25px;
            cursor: pointer;
            font-family: Roboto, sans-serif !important;
            font-weight: 400 !important;
            font-size: 16px !important;
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            transition: all 0.3s;
            border: 0;
            display: inline-block;
            outline: none;
            position: relative;
        }

        .submit-link:hover {
            background: #2ac382 !important;
        }

        .submit-link:active {
            background: #2acb8a !important;
        }
    </style>
@endsection
@section('scripts')
    <script>
        $(document).ready(function () {
            $('.ui.form').form({
                inline: true,
                fields: {
                    user_password: {
                        identifier: 'user_password',
                        rules: [{
                            type: 'empty',
                            prompt: 'Введите новый пароль'
                        }]
                    },
                    user_password_confirmation: {
                        identifier: 'user_password_confirmation',
                        rules: [{
                            type: 'empty',
                            prompt: 'Введите подтверждение пароля'
                        }, {
                            type: 'match[user_password]',
                            prompt: 'Пароли должны совпадать'
                        }]

                    }
                }
            })
            $('.message .close').on('click', function () {
                $(this).closest('.message').transition('fade');
            });
        })
    </script>

    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        let app = new Vue({
            el: '#app',
            data: {
                token: '{{$token}}',
                user_password: '',
                user_password_confirmation: '',
                isLoading: false,
                hasError: false,
                errorText: ''
            },
            methods: {
                checkAndSubmit: function () {
                    this.hasError = false
                    let vue = this

                    if (this.passwordMatch && this.user_password !== '') {
                        this.isLoading = true
                        let data = {
                            '_token': '{{csrf_token()}}',
                            'token': this.token,
                            'user_password': this.user_password
                        };

                        $.ajax({
                            url: ('/auth/reset'),
                            data: data,
                            type: 'post',
                            error: function (data) {
                                vue.hasError = true
                                let response = data['responseJSON']
                                switch (response['error']) {
                                    case "already authenticated":
                                        vue.errorText = 'Вы уже авторизованы в системе'
                                        break;
                                    case "validation error":
                                        vue.errorText = 'Ошибка валидации данных'
                                        break;
                                    case "token was expired":
                                        vue.errorText = 'Время действия вашего кода истекло'
                                        break;
                                }
                            },
                            success: function (response) {
                                //TODO: убрать условие целиком, когда Гоша починит 200 и 400 в контроллере
                                if (response['status'] === 'error') {
                                    vue.hasError = true
                                    switch (response['error']) {
                                        case "already authenticated":
                                            vue.errorText = 'Вы уже авторизованы в системе'
                                            break;
                                        case "validation error":
                                            vue.errorText = 'Ошибка валидации данных'
                                            break;
                                        case "token was expired":
                                            vue.errorText = 'Время действия вашего кода истекло'
                                            break;
                                    }
                                }
                                else{
                                    window.location.href = '/cabinet'
                                }
                            },
                            complete: function () {
                                vue.isLoading = false
                            }
                        })
                    }
                    else {
                        //show message with error
                        $('.ui.form').form('validate form')
                    }
                }
            },
            computed: {
                passwordMatch() {
                    if (this.user_password !== '')
                        return (this.user_password === this.user_password_confirmation)
                    else
                        return true
                }
            }
        })
    </script>

@endsection
<!doctype html>
<html lang="{{app()->getLocale()}}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="_token" content="{{ csrf_token() }}">
    <title>Евразкомпрессор</title>
    <link rel="stylesheet" href="{{asset('semantic/semantic.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/super_responsive.css')}}">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Russo+One&amp;subset=cyrillic" rel="stylesheet">
    @yield('styles')
</head>
<body>
    @yield('content')
    <script src="{{asset('js/jquery-3.2.1.min.js')}}"></script>
    <script src="{{asset('semantic/semantic.min.js')}}"></script>
    {{--<script src="{{ asset('js/app.js') }}" defer></script>--}}

    @yield('scripts')
</body>
</html>
//компонент выпадающего списка для быстрого создания (deprecated)
let dropdown = Vue.component('drop', {
    template: `<div class="ui selection dropdown" ref="input">
                          <input type="hidden" :value="value" @change="updateDrop">
                          <i class="dropdown icon"></i>
                          <div class="default text">{{defaultText}}</div>
                          <div class="menu">
                            <div v-for="item in items" class="item" :data-value="item.value">{{ item.name }}</div>
                          </div>
                    </div>`,

    //передаваемые снаружи параметры
    props: ['value', 'defaultText', "items"],
    methods: {
        updateDrop() {
            let id_value = $(this.$el).find('input').val();
            id_value = (id_value === "" || id_value === null) ? '' : parseInt(id_value);
            if (id_value !== '') {
                this.$emit('input', id_value);
                this.$emit('valuechanged');
            }
        }
    }
});



let vm = new Vue({
    el: '#app',
    data: {
        //переменная для обновления таблица (deprecated)
        updated: true,

        //информация о выбранной модели и марке
        currentModel: '',
        currentModelName: '',
        currentMark: '',
        currentMarkName: '',

        //переменная для хранения параметров модели
        params: [],

        //массив для "Отображать по"
        paginators: [
            {name: 5, value: 5},
            {name: 10, value: 10},
            {name: 50, value: 50},
        ],

        //сколько параметров отображать на экране
        params_limit: 5,

        //сколько параметров всего у модели
        params_total: '',

        //текущая страница
        currentPage: 1,

        //последняя страница
        totalPages: '',

        //показаны параметры "С" "По" (с 1 по 5)
        showedFrom: '',
        showedTo: '',

        //направление сортировки (asc, desc)
        sort_dir: '',
        //название поля сортировки
        sort_field: '',

        //переменная для хранения моделей
        allModels: '',
        //переменная для хранения марок
        allMarks: '',

        //удаляемый параметр
        paramForDelete: '',
        paramForDelete_name:'',

        //операции для фильтрации
        filterOperations: [
            {value: 'eq', text: '=',},
            {value: 'neq', text: '≠'},
            {value: 'gt', text: '>'},
            {value: 'lt', text: '<'},
            {value: 'cont', text: '~'}
        ],
        //текущий открытый фильтр
        currentFilterField: '',

        //запись всех активных фильтров
        //[{name:'field',operation:'eq', value:'value'},{}..]
        allActiveFilters: [],

        //та же запись, но value массивом
        combinedFilter: [],
        allModelsResponse: '',
    },
    computed: {
        _token: function () {
            return $('meta[name="_token"]').attr('content')
        },

        sort_object: function () {
            return {'field': this.sort_field, 'order': this.sort_dir}
        },
        pages() {
            //всего записей
            const totalPages = this.totalPages
            const currentPage = this.currentPage
            //сколько страниц отображать
            const pageSize = 5
            const pageCeil = Math.ceil(pageSize / 2)
            const isOddPageSize = pageSize % 2 === 0

            let startPage, endPage
            if (totalPages <= pageSize) {
                startPage = 1
                endPage = totalPages
            } else {
                if (currentPage <= pageCeil) {
                    startPage = 1
                    endPage = pageSize
                } else if (currentPage + pageCeil - 1 >= totalPages) {
                    startPage = totalPages - pageSize + 1
                    endPage = totalPages
                } else {
                    startPage = currentPage - pageCeil + (isOddPageSize ? 0 : 1)
                    endPage = currentPage + pageCeil - 1
                }
            }

            const length = endPage - startPage + 1
            return Array.from({length}, (v, i) => i + startPage)
        },
        paginateisShowEdge() {
            return this.totalPages > 5
        },
        paginateisShow() {
            return this.totalPages > 1
        },
        filterFieldName() {
            switch (this.currentFilterField) {
                case 'parameter_address':
                    return 'Адрес регистра'

                case 'parameter_name':
                    return 'Название параметра'

                case 'parameter_format':
                    return 'Формат хранения'

                case 'prameter_write_func':
                    return 'Функция записи'

                case 'parameter_read_func':
                    return 'Функция чтения'
                default:
                    return 'Фильтр не выбран'
            }
        },

        currentFilterValue() {

            let filter_index = this.allActiveFilters.findIndex(x => x.field === this.currentFilterField)
            if(filter_index!== -1)
            {
                $('#filter1 .ui.dropdown').dropdown('set selected', this.allActiveFilters[filter_index]['operation'])
                return this.allActiveFilters[filter_index]['value']
            }


            // if (this.allActiveFilters !== {}){
            //     let value = this.allActiveFilters.find(x => x.name.toString() === this.currentFilterField.toString())
            //     if (value !== undefined)
            //         return value.value
            //     return
            // }
            // else
            //     return
        }

    },
    beforeMount() {
        // this.getAllModels()
        let vue = this
        this.getAllMarks()

    },
    methods: {
        getAllModels() {
            let vue = this
            $.ajax({
                url: '/models?value=',
                success: function (data) {
                    vue.allModels = data.results
                }
            })
        },
        getAllMarks() {
            let vue = this
            let newData = []
            $.ajax({
                url: '/marks',
                success: function (data) {
                    $.each(data['results'], function (index, elem) {
                        newData.push({'value': elem.mark_id, 'name': elem.mark_name})
                    });
                    vue.allMarks = newData

                    $('.mark_id').dropdown({
                        fullTextSearch: true,
                        values: vue.allMarks
                    });

                }
            })
        },

        changeModel($event) {

            let newModelId = $event.target.value;
            if (newModelId === '-1') {
                $('.modal[data-modal=add_model]').modal('show')
            }
            else {
                this.currentModel = newModelId
                this.currentModelName = $('#find_model').dropdown('get text')

                this.currentMark = this.allModelsResponse.find(x => x.value.toString() === newModelId.toString()).option
                this.currentMarkName = this.allMarks.find(x => x.value === this.currentMark).name

                //обнулить фильтры и страницы для новой модели
                this.clearParamTable();

                //составить запрос для получения всех параметров модели currentModel
                this.updateParams()
            }
        },

        clearParamTable() {
            this.filters = this.showedFrom = this.showedTo = this.params_total = this.totalPages = '';
            this.currentPage = 1;
            this.params_limit = 5
        },

        showEdit() {
            let modal = $('.ui.modal[data-modal=edit_Model]').modal({autofocus: false})
            modal.find('.mark_id').dropdown('set selected', this.currentModelMark)
            modal.modal('show')
        },
        editModel() {
            //валидация формы и отправка запроса на обновление записи
            let vue = this
            if (!$('#editModelForm').form('is valid')) {
                $('#editModelForm').form('validate form')
            }
            else {
                $.ajax({
                    type: 'PUT',
                    url: '/object_model/' + this.currentModel,
                    data: $('#editModelForm').form('get values'),
                    success: function (data) {
                        //закрыть окошко
                        $('.modal[data-modal=edit_Model]').modal('hide')

                        //TODO: обновить данные в дропе (или модели)

                        // vue.getAllModels()

                        // let name = $('#editModelForm').form('get value', 'model_name')
                        let modelValue = $('#editModelForm').form('get value', 'mark_id'),
                            findedMark = vue.allMarks.find(x => x.value.toString() === modelValue.toString()).name,
                            modelName = $('#editModelForm').form('get value', 'model_name'),
                            newName = findedMark + ' ' + modelName

                        $('#find_model .text').text(newName)
                        //
                        vue.currentModelName = findedMark + ' ' + modelName
                        //
                        // console.log(vue.currentModelName)
                        //
                        // // let newModelName = modelValue+' '+name;
                        // // vue.currentModelName = newModelName
                        //
                        // $('#find_model').dropdown('set text', vue.currentModelName).dropdown('set selected',vue.currentModel)


                        // $('#find_model').dropdown('set text', newModelName)
                    },
                    error: function (response) {
                        alert('Error at console')
                        console.log(response['responseJSON'])
                    }
                })
            }
        },

        confirmModelDelete() {
            $('.ui.modal[data-modal=confirm_delete_model]').modal('show')
        },
        deleteModel() {
            $('.ui.modal[data-modal=confirm_delete_model]').modal('hide')
            let vue = this
            $.ajax({
                url: '/object_model/' + vue.currentModel,
                type: 'delete',
                data: {'_token': vue._token},
                success: function (data) {
                    console.log(data)
                    $('#find_model').dropdown('clear')
                    // vue.getAllModels()
                },
                error: function (data) {
                    console.log(data['responseJSON'])
                }
            })
        },

        showEditParam(paramId) {
            let paramData = this.params.find(x => x.parameter_id === paramId)

            $('#editParamForm').form('set values', paramData)
            $('.modal[data-modal=edit_param]').modal('show')
        },

        editParam() {
            let vue = this

            if (!$('#editParamForm').form('is valid')) {
                $('#editParamForm').form('validate form')
            }
            else {
                //TODO: отправлять только измененные данные
                let values = $('#editParamForm').form('get values')
                values._token = this._token
                // console.log($('#editParamForm').form('get values'))
                $.ajax({
                    url: '/model_parameters/' + vue.currentModel,
                    data: values,
                    type: 'PUT',
                    success: function (data) {
                        console.log(data)
                        $('.modal[data-modal=edit_param]').modal('hide')
                        vue.updateParams()
                    },
                    error(data) {
                        console.log(data['responseJSON'])
                    }
                })
            }
        },
        showDeleteParam(paramId) {
            //TODO: удаление параметра
            //удаление параметра
            this.paramForDelete = paramId


            let paramName = this.params.find(x => x.parameter_id === paramId).parameter_name

            this.paramForDelete_name = paramName

            // $('#paramName').html(paramName)

            $('.modal[data-modal=confirm_delete]').modal('show')


        },
        deleteParam() {
            let vue = this
            $.ajax({
                type: 'delete',
                url: '/model_parameters/' + vue.paramForDelete,
                success: function (data) {
                    console.log(data)
                    $('.modal[data-modal=confirm_delete]').modal('hide')
                    vue.updateParams()
                    vue.paramForDelete = ''
                }
            })
            // this.updateParams()
        },


        //подготовка модального окна для копирования параметра
        showCopyModal() {
            $('.modal[data-modal=copy_params]').modal('show')
            let vue = this
            // models = []
            // $.each(this.allModels, function (index, model) {
            //     if (model.value !== '-1'
            //         && model.value.toString() !== vue.currentModel) {
            //         models.push(model)
            //     }
            // })
            $('.dropdown.id_original').dropdown({
                apiSettings: {
                    url: '/models?value={query}',
                    cache: false,
                    onResponse: function (response) {
                        let newResponse = {success: 'true', results: []}
                        $.each(response.results, function (index, item) {
                            if (item.value !== '-1' && item.value.toString() !== vue.currentModel.toString())
                                newResponse.results.push(item)
                        })
                        return newResponse
                    }
                },
                fullTextSearch: true,
                // values: models
            });

        },
        //само копирование параметров
        copyParams() {

            let vue = this
            $.ajax({
                url: '/object_model/' + this.currentModel,
                type: 'PUT',
                data: $('#copyParamsForm').form('get values'),
                success: function (data) {
                    console.log(data)
                    $('.modal[data-modal=copy_params]').modal('hide')
                    vue.updateParams()
                },
                error: function (response) {
                    console.log(reponse['responseJSON'])
                }
            })

        },

        showAddParam() {
            $('.modal[data-modal=add_param]').modal('show')
        },
        addParam() {
            if (!$('#addParamForm').form('is valid')) {
                $('#addParamForm').form('validate form')
            }
            else {
                let data = $('#addParamForm').form('get values')
                console.log(data)
                let vue = this
                $.ajax({
                    url: '/model_parameters',
                    type: 'post',
                    data: data,
                    success: function (data) {
                        $('.modal').modal('hide')
                        vue.updateParams()
                    },
                    error: function (data) {
                        console.log(data['responseJSON'])
                    }

                })

            }
        },

        addModel() {
            let vue = this
            //проверить валидацию формы и отправить запрос на добавление
            if (!$('#addModelForm').form('is valid')) {
                $('#addModelForm').form('validate form')
            }
            else {
                let data = $('#addModelForm').form('get values');

                $.ajax({
                    url: '/object_model',
                    data: data,
                    type: 'post',
                    success: function (response) {
                        $('.modal').modal('hide')
                        $('#find_model')
                            .dropdown('clear')
                            .dropdown('show')
                        setTimeout(function () {
                            $("#find_model").dropdown('set selected', response.model_id)
                            $('#find_model').dropdown('hide')
                        }, 1000)

                    },
                    error: function (data) {
                        console.log(data['responseJSON'])
                    }
                });
            }

        },


        sort(s) {
            //сортировка работает, только если у выбранной модели есть параметры, иначе нет смысла
            if (this.params.length > 1) {

                //проверка, повторное ли нажатие на поле с названием столбца
                if (this.sort_field !== s) {
                    this.sort_field = s;
                    this.sort_dir = 'asc'
                }
                else {
                    this.sort_dir = (this.sort_dir === 'asc') ? 'desc' : 'asc';
                }

                //обновление таблицы с параметрами
                //обновляются все параметры отображения (фильтры, страницы, сортировки)
                this.updateParams()
            }
            else
                return false
        },

        changePage(page) {
            if (page === this.currentPage)
                return
            this.currentPage = page
            this.updateParams()
        },

        toggle_filter(filter) {
            let vue = this
            this.currentFilterField = filter

            $('#filter1').modal({
                onHide: function () {
                    vue.currentFilterField = ''
                    // $('#filter1 .ui.form').form('clear')
                },
                autofocus: false
            }).modal('show')


        },

        confirmFilter(filter) {

            let fieldName = filter,
                operation = $('#filter1 .ui.dropdown').dropdown('get value'),
                value = ($('#filter1 input[name=value]').val() || 'false'),
                filter_index = this.combinedFilter.findIndex(x => x.field === fieldName)
            if (operation === 'cont')
                value = '%' + value + '%'

            //если такого филда еще нет - добавить (и не хранится)
            if (filter_index === -1) {
                this.combinedFilter.push({"field": fieldName, "value": [value], "operation": operation})

                this.allActiveFilters.push({"field": fieldName, "value": value, "operation": operation})
            }
            else {
                //если такой фильтр есть - или обновить или удалить
                if (value === 'false') {
                    this.combinedFilter.splice(filter_index, 1)
                    this.allActiveFilters.splice(filter_index, 1)

                }
                else {
                    this.combinedFilter[filter_index] = {"field": fieldName, "value": [value], "operation": operation}
                    this.allActiveFilters = {"field": fieldName, "value": value, "operation": operation}
                }
            }
            $('#filter1').modal('hide')
            this.updateParams()
            // console.log(this.currentFilterValue)

        },

        hasFilter(name) {
            if (this.combinedFilter.findIndex(x => x.field === name) !== -1)
                return false
            return true

        },


        updateParams() {
            let vue = this
            let data = {
                page: this.currentPage,
                limit: this.params_limit,
                model_id: this.currentModel,
            }
            if (this.combinedFilter !== []) {
                data['filter'] = JSON.stringify(this.combinedFilter)
            }
            if (this.sort_object.field !== '')
                data['sort'] = JSON.stringify(this.sort_object)

            $.ajax({
                url: '/object_model',
                data: data,
                success: function (data) {
                    vue.params = data.data;
                    vue.showedFrom = (data.from || 0);
                    vue.showedTo = (data.to || 0);
                    vue.params_total = data.total;
                    vue.nextPage = !!(data.next_page_url);
                    vue.prevPage = !!(data.prev_page_url)
                    vue.totalPages = data.last_page
                }
            })
        }
    },
    components: {
        'dropdown': dropdown
    }
})
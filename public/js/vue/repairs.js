$('.modal[data-modal=addOrder]').modal({autofocus:false})
$('.modal[data-modal=addManyOrders]').modal({autofocus:false})
let vm = new Vue({
    el:'#app',
    data:{

    },
    methods:{
        showAddRepairOrder(){
            //показать модальное окно
            $('.modal[data-modal=addOrder]').modal('show')
        },
        showAddManyOrders(){
            $('.modal[data-modal=addManyOrders]').modal('show')
        }
    }
})
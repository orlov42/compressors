let pagination = Vue.component('pagination', {
    name: 'pagination',
    template: `<div class="ui pagination menu" :class="className" v-show="isShow">
                    <a class="item" v-if="isShowEdge" :class="{disabled: currentPage === 1}" @click.prevent="onChange(1)">
                        {{previousText}}
                    </a>
                    <a class="item" v-for="page in pages" :class="{active: page === currentPage}" @click.prevent="onChange(page)">
                        {{page}}
                    </a>
                    <a class="item" v-if="isShowEdge" :class="{disabled: currentPage === totalPages}" @click.prevent="onChange(totalPages)">
                        {{nextText}}
                    </a>
                </div>
               `,
    props: {
        floated: Boolean,
        alwaysShowEdge: Boolean,
        perPage: {
            type: Number,
            default: 10
        },
        total: {
            type: Number,
            require: true
        },
        currentPage: {
            type: Number,
            require: true
        },
        previousText: {
            type: String,
            default: '«'
        },
        nextText: {
            type: String,
            default: '»'
        },
        pageSize: {
            type: Number,
            default: 10
        }
    },
    computed: {
        className() {
            let cx = []

            if (this.floated) {
                cx.push('right', 'floated')
            }

            return cx
        },
        totalPages() {
            return Math.ceil(this.total / this.perPage)
        },
        pages() {
            //всего записей
            const totalPages = this.totalPages
            const currentPage = this.currentPage
            const pageSize = this.pageSize
            const pageCeil = Math.ceil(pageSize / 2)
            const isOddPageSize = pageSize % 2 === 0

            let startPage, endPage
            if (totalPages <= pageSize) {
                startPage = 1
                endPage = totalPages
            } else {
                if (currentPage <= pageCeil) {
                    startPage = 1
                    endPage = pageSize
                } else if (currentPage + pageCeil - 1 >= totalPages) {
                    startPage = totalPages - pageSize + 1
                    endPage = totalPages
                } else {
                    startPage = currentPage - pageCeil + (isOddPageSize ? 0 : 1)
                    endPage = currentPage + pageCeil - 1
                }
            }

            const length = endPage - startPage + 1
            return Array.from({length}, (v, i) => i + startPage)
        },
        isShow() {
            return this.totalPages > 1
        },
        isShowEdge() {
            return this.alwaysShowEdge || this.totalPages > this.pageSize
        }
    },
    data() {
        return {
            startPage: 0,
            endPage: 0
        }
    },
    methods: {
        onChange(page) {

            if (page === this.currentPage)
                return
            this.$emit('change', page)
        }
    }
})

let new_drop = Vue.component('drop', {
    template: `<div class="ui selection dropdown" ref="input">
                          <input type="hidden" :value="value" @change="updateDrop">
                          <i class="dropdown icon"></i>
                          <div class="default text">{{defaultText}}</div>
                          <div class="menu">
                            <div v-for="item in items" class="item" :data-value="item.value">{{ item.name }}</div>
                          </div>
                    </div>`,
    props: ['value', 'defaultText', "items"],
    methods: {
        updateDrop() {
            let id_value = $(this.$el).find('input').val();
            id_value = (id_value === "" || id_value === null) ? '' : parseInt(id_value);
            if (id_value !== '') {
                this.$emit('input', id_value);
                this.$emit('valuechanged');
            }
        }
    }
})

let chartComponent = Vue.component('doughnut-chart', {
    extends: VueChartJs.Doughnut,
    mounted() {
        if (this.$data._chart) {
            this.$data._chart.destroy();
        }
        this.renderChart({
            labels: ["Прошло", "Осталось"],
            datasets: [{
                data: [this.counter, 100 - (this.counter)],
                backgroundColor: ["#ff0011", "#bcb9b4",],
            }]
        }, {
            responsive: true,
            maintainAspectRatio: false,
            legend: {display: false},
            cutoutPercentage: 70,
            elements: {
                center: {
                    //counter - это база с которой начинается отчет, его нужно изменить
                    text: this.counter + '%'
                }
            }
        })
    },
    props: ['counter']
});
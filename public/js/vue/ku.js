Chart.pluginService.register({
    beforeDraw: function (chart) {
        if (chart.config.options.elements.center) {
            //Get ctx from string
            let ctx = chart.chart.ctx;

            //Get options from the center object in options
            let centerConfig = chart.config.options.elements.center;
            let fontStyle = centerConfig.fontStyle || 'Arial';
            let txt = centerConfig.text;
            let color = centerConfig.color || '#000';
            let sidePadding = centerConfig.sidePadding || 20;
            let sidePaddingCalculated = (sidePadding / 100) * (chart.innerRadius * 2)
            //Start with a base font of 30px
            ctx.font = "40px " + fontStyle;

            //Get the width of the string and also the width of the element minus 10 to give it 5px side padding
            let stringWidth = ctx.measureText(txt).width;
            let elementWidth = (chart.innerRadius * 2) - sidePaddingCalculated;

            // Find out how much the font can grow in width.
            let widthRatio = elementWidth / stringWidth;
            let newFontSize = Math.floor(30 * widthRatio);
            let elementHeight = (chart.innerRadius * 2);

            // Pick a new font size so it will not be larger than the height of label.
            let fontSizeToUse = Math.min(newFontSize, elementHeight);

            //Set font settings to draw it correctly.
            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            let centerX = ((chart.chartArea.left + chart.chartArea.right) / 2);
            let centerY = ((chart.chartArea.top + chart.chartArea.bottom) / 2);
            ctx.font = fontSizeToUse + "px " + fontStyle;
            ctx.fillStyle = color;

            //Draw text in center
            ctx.fillText(txt, centerX, centerY);
        }
    }
});
let chartComponent = Vue.component('doughnut-chart', {
    extends: VueChartJs.Doughnut,
    mounted() {
        if (this.$data._chart) {
            this.$data._chart.destroy();
        }
        this.renderChart({
            labels: ["Прошло", "Осталось"],
            datasets: [{
                data: [this.counter, 100 - (this.counter)],
                backgroundColor: ["#ff0011", "#bcb9b4",],
            }]
        }, {
            responsive: true,
            maintainAspectRatio: false,
            legend: {display: false},
            cutoutPercentage: 70,
            elements: {
                center: {
                    //counter - это база с которой начинается отчет, его нужно изменить
                    text: this.counter + '%'
                }
            }
        })
    },
    props: ['counter']
});

let allSP = [
    {
        id: '1',
        name: 'ОО №1',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '2',
        name: 'ОО №2',
        inn: "2234552234",
        kpp: '22223223',
        bank: 'Реквизиты2',
        urAddress: 'Ленина 2',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '3',
        name: 'ОО №3',
        inn: "2234552234",
        kpp: '22223223',
        bank: 'Реквизиты2',
        urAddress: 'Ленина 2',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '4',
        name: 'ОО №4',
        inn: "2234552234",
        kpp: '22223223',
        bank: 'Реквизиты2',
        urAddress: 'Ленина 2',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '5',
        name: 'ОО №5',
        inn: "2234551234",
        kpp: '11123123',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 1',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },

];
let allOP = [
    {
        id: '1',
        name: 'ЭО №1',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '2',
        name: 'ЭО №2',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '3',
        name: 'ЭО №3',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '4',
        name: 'ЭО №4',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },
    {
        id: '5',
        name: 'ЭО №5',
        inn: "2234552234",
        kpp: '1112121',
        bank: 'Реквизиты1',
        urAddress: 'Ленина 1',
        factAddress: 'Мира 2',
        postAddress: '622001 Ленина 48',
        email: 'email@mail.ru', phone: '+799588665'
    },

];
let models = [
    {value: 1, name: 'RS1234114'},
    {value: 2, name: 'KKJ123415'},
    {value: 3, name: 'US12414124'},
    {value: 4, name: 'LP0019234'},
    {value: 5, name: 'AWS8832413'}
];
let allKu = [
    {
        id: '111111',
        workPercent: 47,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 69,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 41,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 83,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 12,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },

    {
        id: '111111',
        workPercent: 44,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 66,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 99,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 56,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 39,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },

    {
        id: '111111',
        workPercent: 50,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 60,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 70,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 80,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 90,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },

    {
        id: '111111',
        workPercent: 50,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 60,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 70,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 80,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 90,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },

    {
        id: '111111',
        workPercent: 45,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 60,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 33,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 77,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 64,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },

    {
        id: '111111',
        workPercent: 50,
        serviceProvider: allSP[0],
        operatingProvider: allOP[0],
        deviceId: "",
        model: models[0],
        date: '01.01.1970',
        deviceNumber: '12345'

    },
    {
        id: '222222',
        workPercent: 60,
        serviceProvider: allSP[1],
        operatingProvider: allOP[1],
        deviceId: 2,
        model: models[1],
        date: '01.01.1980',
        deviceNumber: '3322'

    },
    {
        id: '333333',
        workPercent: 70,
        serviceProvider: allSP[2],
        operatingProvider: allOP[2],
        deviceId: 3,
        model: models[3],
        date: '01.01.1990',
        deviceNumber: '6565'

    },
    {
        id: '444444',
        workPercent: 80,
        serviceProvider: allSP[3],
        operatingProvider: allOP[3],
        deviceId: 1,
        model: models[3],
        date: '01.01.1978',
        deviceNumber: '33213'

    },
    {
        id: '555555',
        workPercent: 90,
        serviceProvider: allSP[4],
        operatingProvider: allOP[4],
        deviceId: 2,
        model: models[4],
        date: '01.01.1976',
        deviceNumber: '1124511'

    },
];


$('#modelDropdown').dropdown({
    values: models,
    placeholder: "Выберите модель",
    fullTextSearch: true
});
$('#editDeviceIdDropdown').dropdown({
    values: [
        {value: 1, name: 'Прибор 1'},
        {value: 2, name: 'Прибор 2'},
        {value: 3, name: 'Прибор 3'}
    ],
    placeholder: "Выберите прибор",
    fullTextSearch: true,
    onChange: function () {
        console.log('changed')
    }
});


//items - перебираемый массив со структурой [{name:'name',value:'value'},{}]

let new_drop = Vue.component('drop', {
    template: `<div class="ui selection dropdown" ref="input">
                          <input type="hidden" :value="value" @change="updateDrop">
                          <i class="dropdown icon"></i>
                          <div class="default text">{{defaultText}}</div>
                          <div class="menu">
                            <div v-for="item in items" class="item" :data-value="item.value">{{ item.name }}</div>
                          </div>
                    </div>`,
    props: ['value', 'defaultText', "items"],
    methods: {
        updateDrop() {
            let id_value = $(this.$el).find('input').val();
            id_value = (id_value === "" || id_value === null) ? '' : parseInt(id_value);
            if (id_value !== '') {
                this.$emit('input', id_value);
                this.$emit('valuechanged');
            }
        }
    }
})

let dropdown = Vue.component('semantic-dropdown', {
    template: `<div class="ui selection dropdown" ref="input">
                          <input type="hidden" :value="value" @change="updateDropdown">
                          <i class="dropdown icon"></i>
                          <div class="default text">{{defaultText}}</div>
                          <div class="menu">
                            <div v-for="(item, key) in items" class="item" :data-value="key">{{ item.id }}</div>
                          </div>
                    </div>`,
    props: ['value', 'defaultText', "items"],
    methods: {
        updateDropdown() {
            let id_value = $(this.$el).find('input').val();
            id_value = (id_value === "" || id_value === null) ? '' : id_value;
            if (id_value !== '') {
                this.$emit('input', allKu[id_value]);
                this.$emit('valuechanged');
            }
        }
    }
});

let pagination = Vue.component('pagination', {
    name: 'pagination',
    template: `<div class="ui pagination menu" :class="className" v-show="isShow">
                    <a class="item" v-if="isShowEdge" :class="{disabled: currentPage === 1}" @click.prevent="onChange(1)">
                        {{previousText}}
                    </a>
                    <a class="item" v-for="page in pages" :class="{active: page === currentPage}" @click.prevent="onChange(page)">
                        {{page}}
                    </a>
                    <a class="item" v-if="isShowEdge" :class="{disabled: currentPage === totalPages}" @click.prevent="onChange(totalPages)">
                        {{nextText}}
                    </a>
                </div>
               `,
    props: {
        floated: Boolean,
        alwaysShowEdge: Boolean,
        perPage: {
            type: Number,
            default: 10
        },
        total: {
            type: Number,
            require: true
        },
        currentPage: {
            type: Number,
            require: true
        },
        previousText: {
            type: String,
            default: '«'
        },
        nextText: {
            type: String,
            default: '»'
        },
        pageSize: {
            type: Number,
            default: 10
        }
    },
    computed: {
        className() {
            let cx = []

            if (this.floated) {
                cx.push('right', 'floated')
            }

            return cx
        },
        totalPages() {
            return Math.ceil(this.total / this.perPage)
        },
        pages() {
            //всего записей
            const totalPages = this.totalPages
            const currentPage = this.currentPage
            const pageSize = this.pageSize
            const pageCeil = Math.ceil(pageSize / 2)
            const isOddPageSize = pageSize % 2 === 0

            let startPage, endPage
            if (totalPages <= pageSize) {
                startPage = 1
                endPage = totalPages
            } else {
                if (currentPage <= pageCeil) {
                    startPage = 1
                    endPage = pageSize
                } else if (currentPage + pageCeil - 1 >= totalPages) {
                    startPage = totalPages - pageSize + 1
                    endPage = totalPages
                } else {
                    startPage = currentPage - pageCeil + (isOddPageSize ? 0 : 1)
                    endPage = currentPage + pageCeil - 1
                }
            }

            const length = endPage - startPage + 1
            return Array.from({length}, (v, i) => i + startPage)
        },
        isShow() {
            return this.totalPages > 1
        },
        isShowEdge() {
            return this.alwaysShowEdge || this.totalPages > this.pageSize
        }
    },
    data() {
        return {
            startPage: 0,
            endPage: 0
        }
    },
    methods: {
        onChange(page) {
            if (page === this.currentPage)
                return
            this.$emit('change', page)
        }
    }
})

let vm = new Vue({
    el: "#app",
    data: {
        activeKu: {},
        activeOrg: '',
        KU_LIST: allKu,
        choosenModelForCopy: '',
        models: [
            {value: 1, name: 'RS1234114'},
            {value: 2, name: 'KKJ123415'},
            {value: 3, name: 'US12414124'},
            {value: 4, name: 'LP0019234'},
            {value: 5, name: 'AWS8832413'}
        ],
        updated: true,

        total: allKu.length,

        paginators: [
            {name: 5, value: 5},
            {name: 10, value: 10},
            {name: 50, value: 50},
        ],

        //сколько строчек в таблице отображать
        pageSize: 5,

        //сколько ссылок пагинации отображать
        pagecount: 5,

        //текущая страница
        currentPage: 1,

        showFrom: 0,
        showTo: 0


    },
    methods: {
        //вызов модального для редактирования информации о КУ
        //вызывается при нажатии на иконку редактирования
        editKuInformation(ku) {
            this.activeKu = ku;
            let vue = this;

            let modal = $('.ui.modal[data-modal=edit]');
            modal.modal({
                autofocus: false,
                selector: {
                    approve: '.main_button',
                },
                onApprove: function () {
                    //валидировать форму
                    //при успехе обновлять данные у выбранной КУ
                    let editedData = $('#formEditKu').form('get values');
                    editedData.serialNumber = vue.activeKu.serialNumber;
                    editedData.workPercent = vue.activeKu.workPercent;
                    editedData.serviceProvider = allSP[editedData.serviceProvider - 1]
                    editedData.operatingProvider = allSP[editedData.operatingProvider - 1]
                    vue.KU_LIST[vue.KU_LIST.indexOf(vue.activeKu)] = editedData
                }
            }).modal('show')
            modal.find('.ui.calendar').calendar({
                type: 'date',
                firstDayOfWeek: 1,
                monthFirst: false,
                formatter: {
                    date: function (date, settings) {
                        if (!date) return '';
                        let day = ("0" + date.getDate()).slice(-2);
                        let month = ("0" + (date.getMonth() + 1)).slice(-2)
                        let year = date.getFullYear();
                        return day + '.' + month + '.' + year;
                    }
                },
                text: window.calendar
            });
            $('#operatingOrgDropdown').dropdown('set selected', ku.operatingProvider.id)
            $('#serviceOrgDropdown').dropdown('set selected', ku.serviceProvider.id)
            $('#modelDropdown').dropdown('set selected', ku.model.value);
            if (ku.deviceId)
                $('#editDeviceIdDropdown').dropdown('set selected', ku.deviceId)
            else
                $('#editDeviceIdDropdown').dropdown('clear')


        },

        //отображение информации об организации
        //вызывается при нажатии любой организации в таблице
        showOrgDetails(org) {
            this.activeOrg = org;
            $('.ui.modal[data-modal=org]').modal('show')
        },

        //функция для отображения модального окна приборов
        //вызывается из таблицы КУ
        showDevice(deviceID) {
            $('.modal[data-modal=show_device]').modal('show');
            $('.modal[data-modal=show_device]')
                .find('.dropdown[name=device-choose]')
                .dropdown('set selected', deviceID)
                .addClass('disabled');

            $('.clear.main_button').css('display', 'none');
            $('.disable.main_button').removeAttr('style')
        },

        initRemoveKu(){
            $('#modalDeleteKU').modal('show')
        },

        //удаление КУ из таблицы объектов
        //вызывается из окна редактирования КУ
        removeKu(ku) {
            let index = this.KU_LIST.findIndex(x => x.id === ku.id)
            console.log(index)
            if (index > -1) {
                $('#modalDeleteKU').modal('hide')
                this.KU_LIST.splice(index, 1);
                let self = this;
                self.updated = false;
                self.activeKu = ''
                Vue.nextTick(function () {
                    self.updated = true;
                })
            }

        },

        //обработка копирования КУ
        //вызывается из кнопки "Копировать КУ"

        initCopyKu() {
            let vue = this;
            $('.ui.modal[data-modal=copy_ku]').modal({
                autofocus: false,
                onHide: function () {
                    vue.choosenModelForCopy = '';
                    $(this).find('.dropdown').dropdown('clear')
                }
            }).modal('show');

        },

        initChooseParam(){
            //показать модальное для выбора параметра
            $('#chooseActiveParam').modal('show')
        },

        //pagination
        changePage(page) {
            this.currentPage = page
            let self = this;
            self.updated = false;
            Vue.nextTick(function () {
                self.updated = true;
            })
        },

        //задел под сортировку
        sortTable(s) {
            if (s === this.currentSort) {
                this.currentSortDir = this.currentSortDir === 'asc' ? 'desc' : 'asc';
            }
            this.currentSort = s;
        }

    },
    computed: {
        //свойство для отображения текущего пути
        currentUrl() {
            return window.location.href;
        },
        sortedKU: function () {
            return this.KU_LIST
            // .sort((a, b) => {
            //     let modifier = 1;
            //     if (this.currentSortDir === 'desc') modifier = -1;
            //     if (a[this.currentSort] < b[this.currentSort]) return -1 * modifier;
            //     if (a[this.currentSort] > b[this.currentSort]) return 1 * modifier;
            //     return 0;
            // })
                .filter((row, index) => {
                    let start = (this.currentPage - 1) * this.pageSize;
                    let end = this.currentPage * this.pageSize;

                    this.showFrom = start + 1;
                    this.showTo = (end > this.total) ? this.total : end;

                    if (index >= start && index < end) return true;
                })
        }

    },
    components: {
        'pagination': pagination,
        'doughnut-chart': chartComponent,
        'semantic-dropdown': dropdown,
        'new-drop': new_drop
    }
});

// let vue_table = new Vue({
//     el:'#main_table'
// })
$(document).ready(function(){

    $('.partners-carousel').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        prevArrow:$('#partners .prev'),
        nextArrow:$('#partners .next'),
        dots:true,
        draggable:false,
        autoplay:true
    });
})
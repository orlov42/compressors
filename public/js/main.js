function initMap() {

    let ku = {lat: 55.157305, lng: 61.3740412};
    let map = new google.maps.Map(document.getElementById('map'), {
        zoom: 17,
        center: ku
    });
    let marker = new google.maps.Marker({
        position: ku,
        map: map
    });

}


$(document).ready(function () {
    $('.ui.checkbox').checkbox();
    $('.ui.dropdown').dropdown()
    $('button[data-modal]').on('click', function () {
        let modal = $(this).data('modal');
        $('.ui.modal.' + modal + '').modal('show')
    })


    //переключение шагов "Как это работает"
    $('a.item').on('click', function () {
        if ($(this).data('step')) {
            if ($('#step_segments').find(".animating").length > 0) {
                // alert('animating')
                return false;
            }
            var step = $(this).data('step');
            $(this)
                .addClass('active')
                .closest('.ui.menu')
                .find('.item')
                .not($(this))
                .removeClass('active')
            ;
            $('.card[data-step]').each(function () {
                if ($(this).hasClass('visible')) {
                    $(this).transition({
                        type: 'fade out',
                        onComplete: function () {
                            $('.card[data-step=' + step + ']').transition('fade in')
                        }
                    })
                }
            })
        }
        else {
            if (!$(this).hasClass('dropdown browse')) {
                $(this)
                    .addClass('active')
                    .closest('.ui.menu')
                    .find('.item')
                    .not($(this))
                    .removeClass('active')
                ;
            }
        }

    })
    $('#menu_segment')
        .visibility({
            type: 'fixed',
        })
    // $('#menu_segment a').on('click',function(){
    //     var idtop = ($($(this).attr("href")).offset().top)-95;
    //     $('html,body').animate({scrollTop: idtop}, 500);
    //     return false;
    // })

    $('.ui.form.login').form({
        inline: true,
        fields: {
            user_email: {
                identifier: 'login',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Введите действительный email'
                    }
                ]
            },
            user_password: {
                identifier: 'password',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Пароль не может быть пустым'
                    }
                ]
            }
        }
    })

    $('.modal.auth .submit-link').on('click', function (e) {
        e.preventDefault()
        let btn = $(this)
        if (!($('.ui.form.login').form('is valid'))) {
            $('.ui.form.login').form('validate form')
            btn.addClass('error')
            //.text('Ошибка')
                .prop('disabled', true)
            setTimeout(function () {
                btn.removeClass('error')
                //.text('Войти')
                    .prop('disabled', false)
            }, 2000)
        }
        else {
            btn.removeClass('error')
                .addClass('loading')
                .prop('disabled', true);
            $.ajax({
                url: ("/auth/login"),
                type: 'post',
                data: $('.ui.form.login').form('get values'),
                error: function (data) {
                    let response = data['responseJSON']
                    btn.removeClass('loading').addClass('error')
                    setTimeout(function () {
                        btn.prop('disabled', false).removeClass('error');
                    }, 1000)

                    $('.ui.form.login').form('add errors', {
                        user_email: 'Ошибка входа',
                    });
                    if (response['seconds']) {
                        $('.ui.form.login').form('add errors', {
                            user_password: 'Подождите ' + response['seconds'] + ' секунд',
                        });
                    }

                },
                success: function (response) {
                    window.location.href = '/cabinet'
                }
            })

        }
    })

    $('.modal.auth').modal({
        onHidden: function () {
            $('.ui.form.login').form('clear')
            $('.ui.form.login .error.message').empty()
        }
    })

    $('.modal.register').modal({
        autofocus: false,
        onHidden: function () {
            $('#register_form').form('clear')
            $('#register_form .error.message').empty()

        }
    })

    $('#register_form').form({
        inline: true,
        on: 'blur',
        fields: {
            user_role:{
                identifier: 'user_role',
                rules: [
                    {
                        type: 'minCount[1]',
                        prompt: 'Выберите тип компании'
                    }
                ]
            },
            company_name: {
                identifier: 'company_name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Название компании не может быть пустым'
                    }
                ]
            },
            company_inn: {
                identifier: 'company_inn',
                rules: [
                    {
                        type: 'regExp[/^(([0-9]{12})|([0-9]{10}))$/]',
                        prompt: 'ИНН должен содержать 10 или 12 цифр'
                    }
                ]
            },
            company_kpp: {
                identifier: 'company_kpp',
                rules: [
                    {
                        type: 'regExp[/^([0-9]{9})$/]',
                        prompt: 'КПП должен содержать 9 цифр'
                    }
                ]
            },
            company_address_ur: {
                identifier: 'company_address_ur',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Юридический адрес не может быть пустым'
                    }
                ]
            },
            company_address_fact: {
                identifier: 'company_address_fact',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Фактический адрес не может быть пустым'
                    }
                ]
            },
            company_bank: {
                identifier: 'company_bank',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Банковские реквизиты не могут быть пустыми'
                    }
                ]
            },
            company_index: {
                identifier: 'company_index',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'Почтовый индекс не может быть пустым'
                    },
                    {
                        type: 'exactLength[6]',
                        prompt: 'Почтовый индекс должен содержать 6 символов'
                    }
                ]
            },
            user_name: {
                identifier: 'user_name',
                rules: [
                    {
                        type: 'empty',
                        prompt: 'ФИО не может быть пустым'
                    }
                ]
            },
            user_phone: {
                identifier: 'user_phone',
                rules: [
                    {
                        type: 'regExp[/([+])([0-9]){11}$/]',
                        prompt: 'Введите телефон в формате +79123456789'
                    }
                ]
            },
            user_email: {
                identifier: 'user_email',
                rules: [
                    {
                        type: 'email',
                        prompt: 'Введите настоящий email'
                    }
                ]
            },
            user_password:{
                identifier:'user_password',
                rules:[{
                    type:'empty',
                    prompt:'Введите пароль'
                }]
            },
            user_password_confirm:{
                identifier:'user_password_confirm',
                rules:[{
                    type:'match[user_password]',
                    prompt:'Пароли должны совпадать'
                }]
            },
            confirmation: {
                identifier: 'confirmation',
                rules: [{type: 'checked', prompt: 'Вы должны согласиться с условиями'}]
            }
        },
        //при неправильном заполнении данных необходимо вновь отцентровать модальное окно регистрации
        onFailure: function () {
            $('.modal.register').modal('refresh')
        }
    })
    $('#register_form').submit(function(e){
        e.preventDefault()

        $('#reg_btn').trigger('click')
        return false
    })
    $('#reg_btn').on('click', function (e) {
        e.preventDefault()
        let btn = $(this)
        if (!($('#register_form').form('is valid'))) {
            $('#register_form').form('validate form')
            btn.addClass('error')
                .prop('disabled', true)
            setTimeout(function () {
                btn.removeClass('error')
                    .prop('disabled', false)
            }, 2000)
        }
        else {
            let registerData = $('#register_form').form('get values')
            console.log(registerData)
            $.ajax({
                url: '/auth/register',
                type: 'post',
                data: registerData,
                error: function (data) {
                    let response = data['responseJSON'];
                    btn.removeClass('loading').addClass('error');
                    setTimeout(function () {
                        btn.prop('disabled', false).removeClass('error');
                    }, 1000);

                    if (response['error'] === 'already authenticated') {
                        $('#register_form').form('add errors', {
                            user_password: 'Вы уже авторизованы в системе',
                        });
                    }

                    if (response['error'] === 'bad user data') {
                        $('#register_form').form('add errors', {
                            user_password: 'Ошибка регистрации',
                        });
                    }

                    if (response['error'] === 'validation error') {
                        $('#register_form').form('add errors', {
                            user_password: 'Ошибка валидации',
                        });
                    }

                },
                success: function (response) {
                    window.location.href = '/cabinet'
                }
            })


        }
    });

    //подгрузка первого заголовка
    $('.lazy_load-1').visibility({
        onPassing: function () {
            setTimeout(function () {
                $('.lazy_load-1').transition('fade down')
            }, 500)
        }
    })

    //подгрузка всех особенностей
    $('#features_container h3').visibility({
        onTopVisible: function () {
            $('#features_container .column.feature').transition({
                interval: 300,
                duration: 300,
                animation: 'fade in'
            })
        }
    })
    $('#stats_segment').visibility({
        onBottomVisible: function () {
            $('#stats_segment .statistic').transition({
                type: 'fade down',
                interval: 500,
                duration: 300,

            })
            var time = 2;
            $('.statistic .value').each(function () {
                var i = 1,
                    num = $(this).data('counter'),
                    step = 1000 * time / num,
                    that = $(this),
                    int = setInterval(function () {
                        if (i <= num) {
                            that.html(i);
                        }
                        else {
                            clearInterval(int);
                        }
                        i++;
                    }, step);
            });
        }
    })

    $('#howitworks .grid').visibility({
        onTopVisible: function () {
            $('#howitworks .menu .item').transition({
                type: 'fade in',
                interval: 300
            })
            setTimeout(function () {
                $('.card[data-step=1]').transition('slide left')
            }, 300)
        }
    })

    // $('#partners').visibility({
    //     onTopVisible:function(){
    //         $('#partners .card').transition({
    //             type:'fade in',
    //             interval:500,
    //             duration:500
    //         })
    //     }
    // })

    $('#contact_form h1.header').visibility({
        onTopVisible: function () {
            $('#contact_form .form .column').transition({
                interval: 300,
                duration: 300,
                animation: 'fade in'
            })
        }
    })


    //маска для поля с инн, можно вводить только цифры (10 или 12)
    $('input[name=company_inn]').mask('0000000000ZZ', {
        translation: {
            'Z': {
                pattern: /[0-9]/,
                optional: true
            }
        }
    });
    $('input[name=company_kpp]').mask('000000000')
    $('input[name=user_phone]').mask('+00000000000')
    $('input[name=post_index]').mask('000000')

    $('#recover_pass').form({
        inline: true,
        fields: {
            user_email: {
                identifier: 'user_email',
                rules: [{
                    type: 'email',
                    prompt: 'Введите действительный email'
                }]
            }
        }
    })

    $('.recover.modal .submit-link').on('click', function (e) {
        let btn = $(this)
        let form = $('#recover_pass')
        e.preventDefault()
        if (!form.form('is valid')) {
            form.form('validate form')
        }
        else {
            btn.addClass('loading')
            $.ajax({
                url: '/auth/forgot',
                type: 'post',
                data: form.form('get values'),
                error: function (data) {
                    btn.removeClass('loading')
                    let response = data['responseJSON'];
                    if (response['error'] === 'already authenticated') {
                        $('#register_form').form('add errors', {
                            user_password: 'Вы уже авторизованы в системе',
                        });
                    }
                    if (response['error'] === 'validation error') {
                        $('#register_form').form('add errors', {
                            user_password: 'Ошибка валидации',
                        });
                    }
                    form.addClass('success')


                },
                success: function (data) {
                    btn.removeClass('loading')

                    console.log(data)

                    //сказать что пришло письмо
                    //зафиксировать email

                }
            })
        }

    })

    $('.recover.modal')
        .modal('attach events', '.auth.modal .recover');


    $('.recover.modal').modal({
        onHidden: function () {
            $('#recover_pass').form('clear')
            $('.recover.modal .error.message').empty()
        }
    })
})
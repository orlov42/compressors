$(document).ready(function () {

    //текст для использования в календаре
    window.calendar = {
        days: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
        months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
        monthsShort: ["Янв", "Фев", "Мрт", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Нбр", "Дек"],
        today: "Сегодня",
        now: "Сейчас",
        am: "AM",
        pm: "PM"
    };

    //занесение текущего активного элемента меню в память
    //для дальнейшего переключения
    let active_item = $('#cabinet_menu .active.item')

    $('#cabinet_menu .item').hover(function () {
        $(this).parent()
            .find('.item')
            .not($(this))
            .removeClass("active")

        $(this).addClass("active")
    })

    //возврат текущего активного пункта меню
    //при выходе курсора за границы меню
    $('#cabinet_menu').mouseleave(function () {
        $(this).find('.item').each(function (index, item) {
            $(item).removeClass("active")
        })
        active_item.addClass('active')
    })

    $('.avatar_border')
        .dimmer({
            on: 'hover'
        });

    //при смене логотипа показывать загрузку
    $(document).on('click', '.avatar_border .upload.icon', function () {
        $(this).replaceWith('<div class="ui loader"></div>')
    })
    $(document).on('click', '.avatar_border div.loader', function () {
        $(this).replaceWith('<i class="big upload icon"></i>')
    })


    $('#messages').popup({
            inline: true,
            hoverable: true,
            on: 'click',
            delay: {
                show: 200,
                hide: 1000
            },

        })

    //минимизация бокового меню
    $('#sgm-notify .sidebar.icon').on('click', function () {
        let menu = $('#cabinet_menu')
        let toc = $('.toc')
        if (toc.hasClass('mini')) {
            toc.removeClass('mini')
            menu.removeClass('icon labeled')
        }
        else {
            toc.addClass('mini')
            menu.addClass('icon labeled')
        }
    })

    $('.ui.dropdown').dropdown();


    $('.table.sortable').tablesort()
    $('.ui.checkbox').checkbox()


    //отключение сортировки на пустых полях
    $('.sortable.table th[empty]').off('click')


    //обработка нажатий на иконки фильтров
    $('.filter.link').on('click',function(e){
        e.preventDefault();

        let filter = $(this).attr('data-filter'),
            modal = $('.modal[data-filter='+filter+']'),
            icon = $(this),
            name = icon.attr('data-name')
        modal.find('[name=filter-name]').text('"'+name+'"')
        modal.modal({
            autofocus:false,
            selector    : {
                approve  : '.main_button'
            },
            onApprove:function(){
                //при нажатии кнопки "Подтвердить" в модальном окне
                //происходит проверка на наличие значения в текстовом поле
                //если значение есть то у соответствующей иконки меняется цвет
                //TODO: применить выбранный фильтр
                if(modal.find('input[name=value]').val()!==''){
                    icon.attr('is-active','true')
                }
                //иначе он меняется обратно на серый цвет
                //TODO: отменить все фильтры по текущей колонке
                else{
                    icon.attr('is-active','false')
                }
            },
            onHide:function(){
                //если окно закрыто с пустым полем ввода
                //иконка становится серой
                //TODO: убрать все фильтры по текущей колонке
                if(modal.find('input[name=value]').val()===''){
                    icon.attr('is-active','false')
                }
                modal.find('.form').form('clear')
            }
        }).modal('show')
        if(modal.find('.ui.calendar').length !== 0){
            modal.find('.ui.calendar').calendar({
                type: 'date',
                firstDayOfWeek: 1,
                monthFirst: false,
                formatter: {
                    date: function (date, settings) {
                        if (!date) return '';
                        let day = ("0" + date.getDate()).slice(-2);
                        let month = ("0" + (date.getMonth() + 1)).slice(-2)
                        let year = date.getFullYear();
                        return day + '.' + month + '.' + year;
                    }
                },
                text: window.calendar
            });
        }
        return false
    });

    $('.modal[data-filter=date] .filter-date').dropdown({
        onChange:function(value){
            if (value==='between'){
                $('.additionalDate').removeAttr("style")
            }
            else{
                $('.additionalDate').css("display",'none')

            }
        }
    });

    $('.ui.calendar[data-type=date]').calendar({
        type: 'date',
        firstDayOfWeek: 1,
        monthFirst: false,

        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                let day = ("0" + date.getDate()).slice(-2);
                let month = ("0" + (date.getMonth() + 1)).slice(-2)
                let year = date.getFullYear();
                return day + '.' + month + '.' + year;
            }
        },
        text: window.calendar
    });



})

$(document).ready(function () {
    $('.tabular.menu .item')
        .tab({
            context: $('#context1')
        });

//обработка ввода в поле для часов регламента
    $('input[name=TO-hours]').on('input change', function () {
        if ($(this).val() != '')
            $(this).parent().removeClass('error')
        else
            $(this).parent().addClass('error')
    })

//удаление верхних вкладок (ТО)
    $(document).on('click', '#context1 .circular.red.label', function () {
        let tab = $(this).parent().attr('data-tab')
        $(this).parent().prev().trigger('click')
        $('#main_TO_menu .item[data-tab=' + tab + ']').remove()

        $('#all_tabs .tab[data-tab=' + tab + ']').remove()
    })

//функция для добавления новой верхней вкладки (ТО)
//отвечает и за соответствующее модальное окно
    function addNewTabTO() {
        let example = $('#example_TO_tab'),
            //какой номер будет у новой вкладки
            count = $('#main_TO_menu .item[data-tab]').length + 1,
            menu = $('#main_TO_menu'),
            tab = 'to' + count
        $('.modal[data-modal=TO-hours]').modal({
            selector: {
                approve: '.actions .main_button'
            },
            onApprove: function (elem) {
                if ($('input[name=TO-hours]').val() == '') {
                    $('input[name=TO-hours]').parent().addClass('error')
                    return false;
                }
                else {
                    var hours = $('input[name=TO-hours]').val()
                    menu.find('.right.menu')
                        .before('<a class="item" data-tab="' + tab + '">TO #' + count + ' (' + hours + ' часов)<div class="ui red basic circular floating label">x</div></a>')
                    example.clone()
                        .removeAttr('id')
                        .attr('data-tab', tab)
                        .appendTo('#all_tabs')
                    $('.grid[data-tab=' + tab + ']')
                        .find('.active').attr('data-tab', tab + '-1')
                    $('.tabular.menu .item')
                        .tab({
                            context: $('#context1')
                        });
                }
            },
            onHidden: function () {
                $('input[name=TO-hours]').val('')
                    .parent().removeClass('error')
            }
        }).modal('show')

    }

    $('a[data-action=create-new-TO]').on('click', addNewTabTO);

//функция для определения последнего номера
//если работ в списке нет - возвращает 0
//
    function countWorks(menu) {
        let result;
        if (menu.find('.item[data-tab]').length > 0)
            result = menu.find('.item[data-tab]').last().attr('data-tab').split('-')[1]
        else
            result = 0
        return result;
    }

//-------------------------------------------------
//TODO:сделать js-обработку для редактирования работы
//-------------------------------------------------

    $(document).on('click', '[data-action=add-work]', function () {
        let link = $(this)
        $('.modal[data-modal=workname]').modal({
            selector: {
                approve: '.actions .main_button'
            },
            onApprove: function () {
                if ($('input[name=TO-workname]').val() == '') {
                    $('input[name=TO-workname]').parent().addClass('error')
                    return false;
                }
                else {
                    let tab = parseInt(countWorks(link.parent())) + 1,
                        TO = link.parent().parent().parent().attr('data-tab'),
                        name = $('input[name=TO-workname]').val()

                    link.before('<a class="item" data-tab="' + TO + '-' + tab + '">' + name + '<i class="close icon"></i></a>')

                    let list = $('<ul class="ui list"></ul>'),
                        comment = $('textarea[name=work-comment]').val()
                    if (comment.split(';').length > 1) {
                        $(comment.split(';')).each(function (index, item) {
                            $(list).append('<li class="item">' + item + '</li>')
                        });
                        link.parent().parent().next()
                            .append('<div class="ui basic segment tab" data-tab="' + TO + '-' + tab + '"><a class="edit_work_link">Редактировать</a></div>')
                        $('.segment[data-tab="' + TO + '-' + tab + '"]').prepend(list)
                    }
                    else {
                        link.parent().parent().next()
                            .append('<div class="ui basic segment tab" data-tab="' + TO + '-' + tab + '">' + comment + '<a class="edit_work_link">Редактировать</a></div>')
                    }
                    $('.tabular.menu .item')
                        .tab({
                            context: $('#context1')
                        });
                }
            },
            onHidden: function () {
                $('input[name=TO-workname]').val('')
                    .parent().removeClass('error')
                $('textarea[name=work-comment]').val('')
            }
        }).modal('show')
    })

//удаление конкретной работы и связанного сегмента
    $(document).on('click', '.item[data-tab] .close.icon', function () {
        let tab = $(this).parent().attr('data-tab')
        $(this).parent().remove()
        $('.segment[data-tab=' + tab + ']').remove()
    })


    let values = [
        {name: 'Модель 1', value: '1', text: 'Модель 1'},
        {name: 'Модель 2', value: '2', text: 'Модель 2'},
        {name: 'Модель 3', value: '3', text: 'Модель 3'}
    ];

    $('#find_model').dropdown({
        values: values,
        onChange: function (value, text, choice) {
            if (value) {
                $('#main_content').show()
                $('#warning_message').hide()
            }
            else {
                $('#main_content').hide()
                $('#warning_message').show()
            }
        },
    });
})



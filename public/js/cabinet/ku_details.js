$(document).ready(function () {

    $('.sync.icon').on('click', function () {
        let icon = $(this);
        let new_icon = $('.icon.check')


        $(this).addClass('loading');
        setTimeout(function () {
            $(icon).hide()
        }, 1000);
        setTimeout(function () {
            $(new_icon).show()
        }, 1100);
        setTimeout(function () {
            $(new_icon).hide();
            $(icon).removeClass('loading').show()
        }, 4000)
    });

    //переключение вкладок с информацией
    $('.ui.secondary.menu .item').tab();

    //работа календарей на вкладках "Таблицы" и "Графики"
    $('#rangestart').calendar({
        type: 'date',
        endCalendar: $('#rangeend'),
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var day = ("0" + date.getDate()).slice(-2);
                var month = ("0" + (date.getMonth() + 1)).slice(-2)
                var year = date.getFullYear();
                return day + '.' + month + '.' + year;
            }
        },
    });
    $('#rangeend').calendar({
        type: 'date',
        startCalendar: $('#rangestart'),
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                var day = ("0" + date.getDate()).slice(-2);
                var month = ("0" + (date.getMonth() + 1)).slice(-2)
                var year = date.getFullYear();
                return day + '.' + month + '.' + year;
            }
        },
    });



    $('.rangestart').calendar({
        endCalendar: $('.rangeend'),
        ampm: false,

        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                let day = ("0" + date.getDate()).slice(-2);
                let month = ("0" + (date.getMonth() + 1)).slice(-2)
                let year = date.getFullYear();
                return day + '.' + month + '.' + year;
            }
        },
    });
    $('.rangeend').calendar({
        startCalendar: $('.rangestart'),
        ampm: false,
        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                let day = ("0" + date.getDate()).slice(-2);
                let month = ("0" + (date.getMonth() + 1)).slice(-2)
                let year = date.getFullYear();

                let hour = ("0" + date.getTime()).slice(-2);

                return day + '.' + month + '.' + year;
            }
        },
    });


    let parameters = [
        {name: 'Параметр 1', value: 0},
        {name: 'Параметр 2', value: 1, selected: true},
        {name: 'Параметр 3', value: 2, selected: true},
        {name: 'Параметр 4', value: 3}
    ];


    $('#dropdownChooseParams').dropdown({
        values: parameters,
        maxSelections: 4,
        message: {
            addResult: 'Добавлен <b>{term}</b>',
            count: '{count} выбрано',
            maxSelections: '{maxCount} параметров максимум',
            noResults: 'Ничего не найдено.'
        }
    });
    $('#dropdownChooseParams').dropdown('set selected', 0)
    $('.choose_params').dropdown({
        values: parameters,
        maxSelections: 4,
        message: {
            addResult: 'Добавлен <b>{term}</b>',
            count: '{count} выбрано',
            maxSelections: '{maxCount} параметров максимум',
            noResults: 'Ничего не найдено.'
        }
    });
    $('.choose_params').dropdown('set selected', 0)

    $('.button[data-action=accept_filter]').on('click', function (e) {

        e.preventDefault();
        let choosenParams = $('#dropdownChooseParams').dropdown('get value').split(',');
        if (choosenParams[0] == "") {
            alert('Не выбраны параметры')
        }

    })
    $('.button[data-action=accept_filter_graphs]').on('click', function (e) {

        e.preventDefault();
        let choosenParams = $('.choose_params').dropdown('get value').split(',');
        if (choosenParams[0] == "") {
            alert('Не выбраны параметры')
        }

    })




});

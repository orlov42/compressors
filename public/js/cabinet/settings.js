$(document).ready(function () {
    $('#editModelForm').form({
        inline: true,
        fields: {
            mark_id: {
                identifier: 'mark_id',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите марку'
                }]
            },
            model_name: {
                identifier: 'model_name',
                rules: [{
                    type: 'empty',
                    prompt: 'Введите название модели'
                }]
            }
        }
    });
    $('#copyParamsForm').form({
        inline: true,
        fields: {
            id_original: {
                identifier: 'id_original',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите устройство'
                }]
            }
        }
    });

    $('#addParamForm').form({
        inline: true,
        fields: {
            parameter_name: {
                identifier: 'parameter_name',
                rules: [{
                    type: 'empty',
                    prompt: 'Введите название параметра'
                }]
            },
            parameter_address: {
                identifier: 'parameter_address',
                rules: [{
                    type: 'empty',
                    prompt: 'Введите адрес регистра'
                }]
            },
            parameter_format: {
                identifier: 'parameter_format',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите формат хранения'
                }]
            },
            parameter_read_func: {
                identifier: 'parameter_read_func',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите функцию чтения'
                }]
            },
            prameter_write_func: {
                identifier: 'parameter_read_func',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите функцию записи'
                }]
            }
        }
    });

    $('#editParamForm').form({
        inline: true,
        fields: {
            parameter_name: {
                identifier: 'parameter_name',
                rules: [{
                    type: 'empty',
                    prompt: 'Введите название параметра'
                }]
            },
            parameter_address: {
                identifier: 'parameter_address',
                rules: [{
                    type: 'empty',
                    prompt: 'Введите адрес регистра'
                }]
            },
            parameter_format: {
                identifier: 'parameter_format',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите формат хранения'
                }]
            },
            prameter_write_func: {
                identifier: 'prameter_write_func',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите функцию записи'
                }]
            },
            parameter_read_func: {
                identifier: 'parameter_read_func',
                rules: [{
                    type: 'minCount[1]',
                    prompt: 'Выберите функцию чтения '
                }]
            }
        }
    });

    $('#find_model').dropdown({
        apiSettings:{
            url:'/models?value={query}',
            cache:false,
            onResponse:function(response){
                //отправляем весь ответ во vue
                vm.allModelsResponse  = response.results
            },
            saveRemoteData:false,
        },
        // onChange:function(value,text,choice){
        //     console.log(choice)
        // },
        allowTab:false,
        ignoreCase:true,
        fullTextSearch: true,
        forceSelection:false,
    });
    //функция для быстрого создания параметров
    const createParam = (param_name, registr_address, storage_format, func_write, func_read) => ({
        param_name,
        registr_address,
        storage_format,
        func_write,
        func_read
    });

    //изначальные параметры для таблицы параметров
    let params = {
        1: [
            createParam('Параметр №1, модель 1', 'R111111', 'String', '01', '02'),
            createParam('Параметр №2, модель 1', 'R111222', 'String', '01', '02')
        ],
        2: [
            createParam('Параметр №1, модель 2', 'R222111', 'String', '01', '02')
        ],
        3: [
            createParam('Параметр №1, модель 3', 'R333111', 'String', '01', '02'),
            createParam('Параметр №2, модель 3', 'R333222', 'String', '01', '02'),
            createParam('Параметр №3, модель 3', 'R333333', 'String', '01', '02'),
        ]
    };


    //настройка модального окна для добавления новой модели в список
    $('.modal[data-modal=add_model]').modal({
        autofocus: false,
        onHide: function () {
            $('#addModelForm').form('reset')
        }
    });

    $('.modal[data-modal=add_param] .ui.dropdown').dropdown();

    //удаление параметра из таблицы (с подтверждением)
    $(document).on('click', '.remove[data-param]', function () {
        let currentParam = $(this).data('param');
        let currentModel = $('#find_model').dropdown('get value');

        let param_name = params[currentModel][currentParam]['param_name']
        $('#confirmation').text('Вы уверены что хотите удалить ' + param_name + " ?")
        //номер выбранного параметра внутри модели
        $('.modal[data-modal=confirm_delete]').find('input[name=currentParam]').val(currentParam)

        $('.modal[data-modal=confirm_delete]').modal('show')

    });
    $('.modal[data-modal=confirm_delete]').modal({
        selector: {
            approve: '.ok.main_button',
            deny: '.cancel.main_button'
        },
        onApprove: function () {
            let currentParam = $(this).find('input[name=currentParam]').val();
            let currentModel = $('#find_model').dropdown('get value');
            params[currentModel].splice(currentParam, 1);
            drawTable(currentModel)
        }
    })
});

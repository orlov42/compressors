$(document).ready(function(){
    $('#find_ku').dropdown({
        forceSelection:false,
        message: {
            noResults     : 'Ничего не найдено.'
        },
        onChange:function(value,text,choice){
            if(value){
                $('#main_table').show()
                $('#warning_message').hide()
            }
            else{
                $('#main_table').hide()
                $('#warning_message').show()
            }
        }
    })
})
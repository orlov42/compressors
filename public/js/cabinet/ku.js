$(document).ready(function () {
    $('.ui.dropdown').dropdown({
        forceSelection: false,
        message: {
            noResults: 'Ничего не найдено.'
        }
    });
    $('.ui.form').form();

    //модальное окно для добавления нового КУ
    $('.ui.modal[data-modal=add_ku]').modal({
        autofocus: false,
        selector: {
            approve: '.main_button',
        },
        onApprove: function () {
            //TODO: проверка формы (нет пустых полей)
            let modal = $('.modal[data-modal=add_ku]');
            //TODO: добавление нового устройства
            modal.find('.dimmer').dimmer('show');
            setTimeout(function () {
                modal.find('.dimmer').dimmer('hide');
                modal.modal('hide');
                modal.find('.form').form('clear')
            }, 2000);
            return false
        },
        onHide:function(){
            $(this).find('.form').form('reset')
        }
    });

    $('.modal[data-modal=add_ku]').find('.dropdown.device').dropdown({
        onChange:function(value,text,choice){
            if (value!=0){
                $('.ku-address').removeClass('hidden')
            }
            else{
                $('.ku-address').addClass('hidden')

            }
        }
    })

    //кнопка для добавления новой КУ
    $('button[data-modal=add_ku]').on('click', function () {
        $('.ui.modal[data-modal=add_ku]').modal('show')
    });

    //кнопка для отображения модального окна с приборами
    let modal_devices = $('.modal[data-modal=show_device]')
    $(modal_devices).modal({
        autofocus: false,
        onHidden: function () {
            clearDeviceForm()
        }
    });

    $('button[data-modal=show_device]').on('click', function () {
        $(modal_devices).modal('show')
    });


    let devices = [
        {
            // "device-ID": "1",
            "device-name": "Первый прибор",
            "device-type": "1",
            "device-imei": "123321123321",
            "timezone": "10.0",
            "operate_registr": "123",
            "control_registr": "123",
            "config_registr": "123",
            "polling_time": "60"
        },
        {
            // "device-ID": "2",
            "device-name": "Второй прибор",
            "device-type": "1",
            "device-imei": "321321321312231",
            "timezone": "3.0",
            "operate_registr": "345",
            "control_registr": "345",
            "config_registr": "345",
            "polling_time": "90"
        },
        {
            // "device-ID": "3",
            "device-name": "Третий прибор",
            "device-type": "1",
            "device-imei": "444534353453",
            "timezone": "2.0",
            "operate_registr": "567",
            "control_registr": "567",
            "config_registr": "567",
            "polling_time": "30"
        }
    ];

    function clearDeviceForm() {
        modal_devices.find('form').form('clear');
        $('#device-type').dropdown('restore defaults').removeClass('disabled');
        $('.dropdown.timezone').removeClass('disabled');
        $(modal_devices).find('.dropdown[name=device-choose]').removeClass('disabled').dropdown('restore defaults');

        //removed READONLY
        // modal_devices.find('.field input').removeAttr('readonly')

        modal_devices.find('.field').removeClass('disabled')

        $('.disable.main_button').css('display','none')
        $('.clear.main_button').removeAttr('style')
    }

    $(modal_devices).find('.dropdown[name=device-choose]').dropdown({
        onChange: function (value, text, choice) {
            if (value) {
                modal_devices.find('form').form('set values', devices[value - 1])
                // $('#device-type').dropdown('set selected', 1).addClass('disabled');
                $('#device-type').dropdown('set selected', 1)

                // $('.dropdown.timezone').addClass('disabled')

                $('.imei').removeClass('hidden')

                // modal_devices.find('.field').addClass('disabled')
            }

            //выбрали опцию "добавить прибор
            else{
                modal_devices.find('form').form('clear')
                $('.imei').addClass('hidden')

            }
        }
    });

    $(modal_devices).find('button.clear').on('click', function () {
        clearDeviceForm()
    });

    $('#timezone').dropdown();
    $('#device-type').dropdown({
        onChange: function (value, text, choice) {
            if (value)
                $('.imei.row').removeClass('hidden')

            else
                $('.imei.row').addClass('hidden')

        }
    });
    clearDeviceForm();


    $('.disable.main_button').on('click',function(){
        //показать модальное для подтверждения отвязки прибора
        $('.modal[data-modal=confirmDeleteDevice]').modal({
            onDeny:function(){

            }
        }).modal('show')
    })

    //отображение календаря для даты
    $('.ui.calendar[data-type=date]').calendar({
        type: 'date',
        firstDayOfWeek: 1,
        monthFirst: false,

        formatter: {
            date: function (date, settings) {
                if (!date) return '';
                let day = ("0" + date.getDate()).slice(-2);
                let month = ("0" + (date.getMonth() + 1)).slice(-2)
                let year = date.getFullYear();
                return day + '.' + month + '.' + year;
            }
        },
        text: window.calendar
    });

});


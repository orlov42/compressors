$(document).ready(function () {
    let animating = false,
        slider = $('#slick-slider'),
        count = parseInt(slider.find('.column[data-slide]').length)-1,
        firstSlide = $('[data-slide=0]'),
        lastSlide = $('[data-slide=' + count + ']');

    function makeVisible(prev, next) {
        $(prev).transition({
            animation: 'fade',
            onStart:function(){
                animating = true;
            },
            onComplete: function () {
                $(next).transition('fade')
                animating = false;
            }
        })
    }

    $('.arrows').on('click', function () {
            if (animating) {
                return false;
            }
            else {
                let current=$('.visible[data-slide]')
                if ($(this).hasClass('right')) {
                    if(current.data('slide') === lastSlide.data('slide'))
                        makeVisible(current, firstSlide)
                    else{
                        let newIndex = parseInt(current.data('slide'))+1,
                            nextSlide = $('[data-slide='+newIndex+']');
                        makeVisible(current, nextSlide)
                    }
                }
                else if ($(this).hasClass('left')) {
                    if(current.data('slide') === firstSlide.data('slide'))
                        makeVisible(current, lastSlide)
                    else{
                        let newIndex = parseInt(current.data('slide'))-1,
                            nextSlide = $('[data-slide='+newIndex+']');
                        makeVisible(current, nextSlide)
                    }
                }


            }

        }
    )
});
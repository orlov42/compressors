<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{

    protected $table = 'users';
    public $timestamps = true;
    protected $primaryKey = 'user_id';

    use SoftDeletes;

    protected $dates = ['deleted_at', 'created_at', 'updated_at'];
    protected $fillable = array('user_name', 'user_email', 'user_phone', 'user_role', 'user_password');
    protected $visible = array('user_name', 'user_email', 'user_phone', 'user_role', 'user_status');
    protected $hidden = array('user_id', 'user_password', 'created_at', 'updated_at', 'deleted_at');

    public function getCompany()
    {
        return $this->hasOne('App\Models\Company', 'user_id','user_id');
    }
}
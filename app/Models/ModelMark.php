<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelMark extends Model 
{

    protected $table = 'model_marks';
    public $timestamps = false;
    protected $fillable = array('mark_name');
    protected $visible = array('mark_id', 'mark_name');

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeviceType extends Model 
{

    protected $table = 'device_types';
    public $timestamps = false;
    protected $fillable = array('device_type_name');
    protected $visible = array('device_type_name');

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectModel extends Model 
{

    protected $table = 'object_models';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('mark_id', 'model_name', 'model_work_register');
    protected $visible = array('model_id', 'mark_id', 'model_name', 'model_work_register');
    protected $hidden = array('');

    public function getParameters()
    {
        return $this->belongsTo('App\Models\ModelParameter', 'model_id', 'model_id');
    }

    public function getMark()
    {
        return $this->hasOne('ModelMark', 'mark_id', 'mark_id');
    }

}
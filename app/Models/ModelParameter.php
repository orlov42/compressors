<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModelParameter extends Model 
{

    protected $table = 'model_parameters';
    protected $primaryKey = 'parameter_id';
    public $timestamps = false;
    protected $fillable = array('model_id', 'parameter_name', 'parameter_address', 'parameter_format', 'parameter_read_func', 'prameter_write_func', 'prameter_work', 'prameter_danger');
    protected $visible = array('parameter_id', 'parameter_name', 'parameter_address', 'parameter_format', 'parameter_read_func', 'prameter_write_func', 'prameter_work', 'prameter_danger');
    protected $hidden = array('model_id', 'parameter_control');

}
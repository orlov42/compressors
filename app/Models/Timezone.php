<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timezone extends Model 
{

    protected $table = 'timezones';
    public $timestamps = false;
    protected $fillable = array('timezone_text', 'timezone_int');
    protected $visible = array('timezone_id', 'timezone_text', 'timezone_int');

}
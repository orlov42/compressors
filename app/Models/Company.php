<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Company extends Model 
{

    protected $table = 'companies';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('user_id', 'company_name', 'company_inn', 'company_kpp', 'company_address_ur', 'company_address_fact', 'company_bank', 'company_index');
    protected $visible = array('company_name', 'company_inn', 'company_kpp', 'company_address_ur', 'company_address_fact', 'company_bank', 'company_index');
    protected $hidden = array('company_id', 'user_id');

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ObjectData extends Model
{

    protected $table = 'objects';
    public $timestamps = true;
    protected $primaryKey = 'object_id_serial';
    public $incrementing = false;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('object_id_serial', 'model_id', 'company_id_service', 'company_id_explot', 'object_create_date', 'object_work_hours');
    protected $visible = array('object_id_serial', 'model_id', 'company_id_service', 'company_id_explot', 'object_create_date', 'object_work_hours');

    public function getModelData()
    {
        return $this->hasOne('App\Models\ObjectModel', 'model_id', 'model_id');
    }

    public function getDevice()
    {
        return $this->hasOne('App\Models\Device', 'object_id_serial', 'object_id_serial');
    }

}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PasswordReset extends Model
{

    protected $table = 'password_resets';
    public $timestamps = false;
    protected $primaryKey = 'user_email';
    public $incrementing = false;

    protected $dates = ['created_at'];
    protected $fillable = array('user_email', 'token','created_at');
    protected $visible = array('user_email','created_at','token');
//    protected $hidden = array('token');

}
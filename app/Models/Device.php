<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Device extends Model 
{

    protected $table = 'devices';
    public $timestamps = true;

    use SoftDeletes;

    protected $dates = ['deleted_at'];
    protected $fillable = array('timezone_id', 'device_type_id', 'device_name', 'device_imei_mac', 'device_unit_id', 'device_options', 'request_time', 'config_time', 'working_time', 'change_status', 'request', 'object_id_serial', 'model_work_register', 'device_need_request');
    protected $visible = array('device_type_id', 'device_name', 'device_imei_mac', 'device_unit_id', 'device_options', 'request_time', 'config_time', 'working_time', 'change_status', 'request', 'object_id_serial', 'model_work_register', 'device_need_request');
    protected $hidden = array('timezone_id');

    public function getTimezone()
    {
        return $this->hasOne('App\Models\Timezone', 'timezone_id', 'timezone_id');
    }

    public function getDeviceType()
    {
        return $this->hasOne('App\Models\DeviceType', 'device_type_id', 'device_type_id');
    }

}
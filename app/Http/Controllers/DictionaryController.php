<?php

namespace App\Http\Controllers;

use App\Models\ModelMark;
use App\Models\ObjectModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DictionaryController extends Controller
{
    //Получить все марки моделей КУ
    public function getMarks()
    {
        if(Auth::check())
        {
            $marks = ModelMark::all();
            return response()->json(['success'=>'true','results'=>$marks]);
        }
        else
        {
            return response()->json(["status"=>"error", "error"=>"user not auth"], 400);
        }

    }
    //Получить посимвольно все модели КУ
    public function getModelsBySymbols(Request $request)
    {
        if(Auth::check())
        {
            if($request->has('value'))
            {
                $value = $request->input('value');
                $models = ObjectModel::join('model_marks', 'object_models.mark_id', '=', 'model_marks.mark_id')->where('model_name', 'LIKE',  "%$value%")->limit(100)->get();
                $result = [];
                //добавил пустую строчку для дропа
                array_push($result,['value'=>'-1','name'=>'+ Добавить модель', "option"=>'-1']);
                foreach ($models as $model)
                {
                    array_push($result, ["value"=>$model["model_id"], "name"=>$model["mark_name"]." ".$model["model_name"], "option"=>$model["mark_id"]]);
                }
                return response()->json(["success"=>"true", "results"=>$result]);
            }
        }
        else
        {
            return response()->json(["status"=>"error", "error"=>"user not auth"], 400);
        }

    }
}

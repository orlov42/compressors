<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\PasswordReset;
use App\Models\User;
use DateTime;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    /**
     * Сброс пароля
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        if ($this->guard()->check())
            return new JsonResponse(["status" => "error", "error" => "already authenticated"], 400);
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return new JsonResponse(["status" => "error", "error" => "validation error"], 400);
        }
        $token = $request->get('token', '');
        $new_password = $request->get('user_password', null);
        $reset = PasswordReset::where('token', $token)->first();
        if (is_null($reset) || is_null($new_password))
            return new JsonResponse(["status" => "error", "error" => "token was expired"], 400);
        $dt = new DateTime();
        $minutes = $dt->diff($reset->created_at)->m;
        if ($minutes > 30)
            return new JsonResponse(["status" => "error", "error" => "token was expired"], 400);
        $reset->forceDelete();

        $user = User::where('user_email', $reset->user_email)->first();
        if (is_null($user))
            return new JsonResponse(["status" => "error", "error" => "token was expired"], 400);
        try {
            $user->user_password = bcrypt($new_password);
            $user->save();
        } catch (\Exception $e) {
            return new JsonResponse(["status" => "error", "error" => "token was expired"], 400);
        }
        $this->guard()->login($user);
        return new JsonResponse(["status" => "ok"], 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'token' => 'required|string',
            'user_password' => 'required|string'
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}

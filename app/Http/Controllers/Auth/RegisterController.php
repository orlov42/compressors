<?php

namespace App\Http\Controllers\Auth;

use App\Models\Company;
use App\Models\User;
use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Foundation\Auth\RedirectsUsers;
use Illuminate\Auth\Events\Registered;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | Отвечает за регистрацию всех типов пользователей
    |
    */

//    use RedirectsUsers;
//
//    /**
//     * Перенаправление пользователей на главную после регистрации
//     *
//     * @var string
//     */
//    protected $redirectTo = '/';
//
//    /**
//     * Перенаправление пользователей на главную если уже авторизован
//     *
//     */
//    public function __construct()
//    {
//        $this->middleware('guest');
//    }

    /**
     * Регистрация пользователя
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        if ($this->guard()->check())
            return new JsonResponse(["status" => "error", "error" => "already authenticated"], 400);
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return new JsonResponse(["status" => "error", "error" => "validation error"], 400);
        }

        $user = $this->create($request->all());
        if (is_null($user))
            return new JsonResponse(["status" => "error", "error" => "bad user data"], 400);
        $this->guard()->login($user);
        return new JsonResponse(["status" => "ok"], 200);


//        event(new Registered($user = $this->create($request->all())));

//        return redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_name' => 'required|string',
            'user_email' => 'required|email|unique:users',
            'user_phone' => 'required|string',
            'user_password' => 'required|string'
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        $user = null;
        $user_role = 0;
        if (key_exists("user_role", $data))
            $user_role = $data["user_role"];

        try {
            $user =  User::create([
                'user_name' => $data['user_name'],
                'user_email' => $data['user_email'],
                'user_phone' => $data['user_phone'],
                'user_password' => bcrypt($data['user_password']),
                'user_role' => $user_role,
                'user_status' => 2
            ]);

            Company::create([
                'user_id'=>$user->user_id,
                'company_name' => $data['company_name'],
                'company_inn'=> $data['company_inn'],
                'company_kpp'=> $data['company_kpp'],
                'company_address_ur'=> $data['company_address_ur'],
                'company_address_fact'=> $data['company_address_fact'],
                'company_bank'=> $data['company_bank'],
                'company_index'=> $data['company_index']
            ]);

            return $user;
        } catch (Exception $e) {
            if(!is_null($user))
                $user->forceDelete();
            return null;
        }
    }
}

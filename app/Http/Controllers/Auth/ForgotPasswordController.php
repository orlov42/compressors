<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\ForgotPassword;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    /**
     * Восстановление пароля
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgot(Request $request)
    {
        if ($this->guard()->check())
            return new JsonResponse(["status" => "error", "error" => "already authenticated"], 400);
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            return new JsonResponse(["status" => "error", "error" => "validation error"], 400);
        }

        $user_email = $request->get('user_email', '');

        $user = User::where('user_email', $user_email)->first();

        if (is_null($user))
            return new JsonResponse(["status" => "ok"], 200);
        $token = str_random(128);

        PasswordReset::updateOrCreate([
            'user_email' => $user_email,
            'token' => $token,
            'created_at' => date("Y-m-d H:i:s")
        ]);

        Mail::to($user_email)->send(new ForgotPassword($token));
        return new JsonResponse(["status" => "ok"], 200);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'user_email' => 'required|email',
        ]);
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }
}

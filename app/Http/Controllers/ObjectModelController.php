<?php

namespace App\Http\Controllers;

use App\Models\ModelParameter;
use App\Models\ObjectModel;
use Illuminate\Http\Request;
use League\Flysystem\Exception;
use phpDocumentor\Reflection\Types\Null_;
use PhpParser\Node\Expr\AssignOp\Mod;

class ObjectModelController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Отобразить параметры в таблице пагинация
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //between обрабатывается двумя фильтрами по одному полю больше и меньше
        $operation = ["eq"=>"=", "neq"=>"<>", "lt"=>"<", "gt"=>">", "cont"=>"like"];
        $query = ModelParameter::query();
        $per_page = $request->input("limit");
        //$page = $request->input("page");
        $model_id = $request->input("model_id");
        if($request->has("filter"))
        {
            try
            {
                $filter = json_decode($request->input("filter"), true);
                $where = [];
                foreach ($filter as $arr)
                {
                    array_push($where, [$arr["field"], $operation[$arr["operation"]], $arr["value"]]);
                }
                array_push($where, ["model_id", $model_id]);
                $query->where($where);
            }
            catch (Exception $ex) {};
        }
        else
        {
            $query->where("model_id", $model_id);
        }
        if($request->has("sort"))
        {
            $sort = json_decode($request->input("sort"), true);
            $query->orderBy($sort["field"], $sort["order"]);
        }
        return $query->paginate($per_page);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Создание модели КУ.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try
        {
            if($request->has('action'))
            {
                $close = 1;
                /*if($request->input('action')=="copy")
                {
                    $copy_id = $request->input('id_copy');
                    $original_id = $request->input('id_original');
                    $params = ModelParameter::where("model_id", $copy_id)->get();
                    foreach ($params as $param)
                    {
                        $attributes = $param->toArray();
                        $attributes["model_id"] = $original_id;
                        ModelParameter::create($attributes);
                    }
                    return response()->json(["status"=>"success"], 200);
                }*/
            }
            else
            {
                $input = $request->except(['_token']);
                $model = ObjectModel::create($input);
                return response()->json(["status"=>"success", "model_id"=>$model["id"]], 200);
            }
        }
        catch (Exception $ex) {return response()->json(["error"=>"not add", "status"=>"error"], 419);}
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Редактирование названия модели
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        try
        {
            if($request->has('action'))
            {
                if($request->input('action')=="copy")
                {
                    $copy_id = $request->input('id_copy');
                    $original_id = $id;//$request->input('id_original');
                    $params = ModelParameter::where("model_id", $copy_id)->get();
                    if(count($params)!=0)
                    {
                        ModelParameter::where("model_id", $original_id)->forceDelete();
                        foreach ($params as $param)
                        {
                            $attributes = $param->toArray();
                            $attributes["model_id"] = $original_id;
                            ModelParameter::create($attributes);
                        }
                        return response()->json(["status"=>"success"], 200);
                    }
                    else
                    {
                        return response()->json(["status"=>"success", "info"=>"empty parameters"], 200);
                    }

                }
            }
            else
            {
                $name = $request->input("model_name");
                $mark = $request->input("mark_id");
                if(is_null($name)) $name = "";
                ObjectModel::where('model_id', $id)->update(["model_name"=>$name, "mark_id"=>$mark]);
                return response()->json(["status"=>"success"], 200);
            }
        }
        catch (Exception $ex) {return response()->json(["error"=>"not upd", "status"=>"error"], 400);}
    }

    /**
     * Удалить модель и все её параметры
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        try
        {
            //Мягкое удаление не стал использовать, если нужно будет
            //то необходимо добавить его в model_parameters
            //и добавить функцию удаления связанных записей
            $del = ObjectModel::where('model_id', $id)->forceDelete();
            return response()->json(["status"=>"success"], 200);
        }
        catch (Exception $ex) {return response()->json(["error"=>"not delete", "status"=>"error"], 400);}
    }
}

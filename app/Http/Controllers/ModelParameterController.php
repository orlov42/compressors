<?php

namespace App\Http\Controllers;

use App\Models\ModelParameter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use League\Flysystem\Exception;

class ModelParameterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Добавление нового параметра для модели КУ
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        try
        {
            $input = $request->except(['_token']);
            ModelParameter::create($input);
            return response()->json(["status"=>"success"], 200);
        }
        catch (Exception $ex) {return response()->json(["error"=>"not add", "status"=>"error"], 419);}
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ModelParameter  $modelParameter
     * @return \Illuminate\Http\Response
     */
    public function show(ModelParameter $modelParameter)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ModelParameter  $modelParameter
     * @return \Illuminate\Http\Response
     */
    public function edit(ModelParameter $modelParameter)
    {
        //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ModelParameter  $modelParameter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id/*, ModelParameter $modelParameter*/)
    {
        //
//        $modelParameter = new ModelParameter;
//        foreach($request->except(['_token']) as $key=>$item)
//        {
//            $modelParameter[$key] = $item;
//        }
//        $modelParameter->save();
        try
        {
            ModelParameter::where('parameter_id', $id)->update($request->except(['_token']));
            return response()->json(["status"=>"success"], 200);
        }
        catch (Exception $ex) {return response()->json(["error"=>"not upd", "status"=>"error"], 400);}
    }

    /**
     * Удалить параметр
     *
     * @param  \App\Models\ModelParameter  $modelParameter
     * @return \Illuminate\Http\Response
     */
    public function destroy($id/*ModelParameter $modelParameter*/)
    {
        //
        try
        {
            ModelParameter::where('parameter_id', $id)->delete();
            return response()->json(["status"=>"success"], 200);
        }
        catch (Exception $ex) {return response()->json(["error"=>"not delete", "status"=>"error"], 400);}
    }
}
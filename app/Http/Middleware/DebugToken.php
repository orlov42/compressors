<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class DebugToken
{
    /**
     * Защита сайта при разработке.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!env("APP_DEBUG",true))
            return $next($request);

        $token = env("DEBUG_TOKEN");

        if($request->path()===("debug_token/".$token)) {
            $response = redirect("/");
            $response->withCookie(cookie()->forever('debug_token', $token));
            return $response;
        }

        if ($request->cookie("debug_token","") === $token)
            return $next($request);
        return \response("",200);
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModelMarksTable extends Migration {

	public function up()
	{
		Schema::create('model_marks', function(Blueprint $table) {
			$table->increments('mark_id');
			$table->string('mark_name');
		});
	}

	public function down()
	{
		Schema::drop('model_marks');
	}
}
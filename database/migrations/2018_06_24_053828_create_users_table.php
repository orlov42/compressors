<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
			$table->bigIncrements('user_id');
			$table->string('user_name');
			$table->string('user_email')->unique();
			$table->string('user_phone');
			$table->string('user_password');
			$table->tinyInteger('user_role')->unsigned()->default(0);
			$table->tinyInteger('user_status')->unsigned()->default(0);
			$table->rememberToken();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('users');
	}
}
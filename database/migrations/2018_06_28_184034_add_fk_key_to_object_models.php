<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFkKeyToObjectModels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('model_parameters', function (Blueprint $table) {
            //
            //$table->unsignedInteger('user_id');

            $table->foreign('model_id')->references('model_id')->on('object_models')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('model_parameters', function (Blueprint $table) {
            //
            $table->dropForeign(['model_id']);
        });
    }
}

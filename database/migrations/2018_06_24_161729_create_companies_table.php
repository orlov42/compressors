<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompaniesTable extends Migration {

	public function up()
	{
		Schema::create('companies', function(Blueprint $table) {
			$table->bigIncrements('company_id');
			$table->bigInteger('user_id')->unique()->unsigned();
			$table->string('company_name')->nullable();
			$table->string('company_inn')->nullable();
			$table->string('company_kpp')->nullable();
			$table->string('company_address_ur')->nullable();
			$table->string('company_address_fact')->nullable();
			$table->string('company_bank')->nullable();
			$table->string('company_index')->nullable();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::dropIfExists('companies');
	}
}
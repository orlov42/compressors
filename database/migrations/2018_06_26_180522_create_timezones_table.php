<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimezonesTable extends Migration {

	public function up()
	{
		Schema::create('timezones', function(Blueprint $table) {
			$table->increments('timezone_id');
			$table->string('timezone_text');
			$table->integer('timezone_int');
		});
	}

	public function down()
	{
		Schema::drop('timezones');
	}
}
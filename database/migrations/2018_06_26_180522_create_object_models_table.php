<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectModelsTable extends Migration {

	public function up()
	{
		Schema::create('object_models', function(Blueprint $table) {
			$table->bigIncrements('model_id');
			$table->integer('mark_id')->index();
			$table->string('model_name');
			$table->tinyInteger('model_work_register')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('object_models');
	}
}
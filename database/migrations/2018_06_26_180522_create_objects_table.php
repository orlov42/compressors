<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateObjectsTable extends Migration {

	public function up()
	{
		Schema::create('objects', function(Blueprint $table) {
			$table->string('object_id_serial')->unique();
			$table->bigInteger('model_id')->index();
			$table->bigInteger('company_id_service')->unsigned()->index();
			$table->bigInteger('company_id_explot')->unsigned()->index();
			$table->date('object_create_date');
			$table->integer('object_work_hours')->unsigned();
			$table->timestamps();
			$table->softDeletes();
		});
	}

	public function down()
	{
		Schema::drop('objects');
	}
}
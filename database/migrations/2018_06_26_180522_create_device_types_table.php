<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDeviceTypesTable extends Migration {

	public function up()
	{
		Schema::create('device_types', function(Blueprint $table) {
			$table->bigIncrements('device_type_id');
			$table->string('device_type_name');
		});
	}

	public function down()
	{
		Schema::drop('device_types');
	}
}
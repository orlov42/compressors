<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDevicesTable extends Migration {

	public function up()
	{
		Schema::create('devices', function(Blueprint $table) {
			$table->bigIncrements('device_id');
			$table->integer('timezone_id')->unsigned()->index();
			$table->tinyInteger('device_type_id')->unsigned()->index();
			$table->string('device_name');
			$table->string('device_imei_mac')->index();
			$table->tinyInteger('device_unit_id');
			$table->text('device_options');
			$table->timestamps();
			$table->softDeletes();
			$table->integer('request_time')->unsigned();
			$table->integer('config_time')->unsigned();
			$table->tinyInteger('change_status');
			$table->binary('request');
			$table->string('object_id_serial');
			$table->tinyInteger('model_work_register')->unsigned();
			$table->tinyInteger('device_need_request')->default('0');
		});
	}

	public function down()
	{
		Schema::drop('devices');
	}
}
<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateModelParametersTable extends Migration {

	public function up()
	{
		Schema::create('model_parameters', function(Blueprint $table) {
			$table->bigIncrements('parameter_id');
			$table->bigInteger('model_id')->unsigned()->index();
			$table->string('parameter_name');
			$table->string('parameter_address');
			$table->tinyInteger('parameter_format');
			$table->tinyInteger('parameter_read_func');
			$table->tinyInteger('prameter_write_func');
			$table->float('parameter_control')->nullable();
		});
	}

	public function down()
	{
		Schema::drop('model_parameters');
	}
}
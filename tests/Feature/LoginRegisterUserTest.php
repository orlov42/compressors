<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LoginRegisterUserTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test()
    {
        $this->refreshApplication();

        $response = $this
            ->json('POST',
                '/auth/register', [
                    'user_name' => 'Супер Программист Тестировщик',
                    'user_email' => 'test@email.com',
                    'user_phone' => '+798151654651',
                    'user_password' => '123456',
                    'company_name' => 'Компания Добра',
                    'company_inn' => '51564123164',
                    'company_kpp' => '5412358',
                    'company_address_ur' => 'Патапаповка, юр. Адрес',
                    'company_address_fact' => 'Патапаповка, факт. Адрес',
                    'company_bank' => 'Убирческий банк',
                    'company_index' => '622077'
                ]
            );

        $response->assertStatus(200);
        $this->assertAuthenticated();

        $response = $this->get('auth/logout');
        $response->assertStatus(200);
        $this->assertGuest();

        $response = $this->json('POST',
                '/auth/login', [
                    'user_email' => 'test@email.com',
                    'user_password' => '123456'
                ]
            );
        $response->assertStatus(200);
        $this->assertAuthenticated();

        $this->assertDatabaseHas('users', [
            'user_email' => 'test@email.com',
        ]);
        $this->assertDatabaseHas('companies', [
            'company_name' => 'Компания Добра'
        ]);

        $response = $this->get('auth/logout');
        $response->assertStatus(200);

        $response = $this
            ->json('POST',
                '/auth/register', [
                    'user_name' => 'Супер Программист Верстак',
                    'user_email' => 'max422356@gmail.com',
                    'user_phone' => '+79825252525',
                    'user_password' => '123456',
                    'company_name' => 'Компания Верстаков',
                    'company_inn' => '1111111111',
                    'company_kpp' => '111111',
                    'company_address_ur' => 'Верстакова, юр. Адрес',
                    'company_address_fact' => 'Верстакова, факт. Адрес',
                    'company_bank' => 'Верстаковский банк',
                    'company_index' => '622111'
                ]
            );
        $response->assertStatus(200);
        $this->assertAuthenticated();
    }
}

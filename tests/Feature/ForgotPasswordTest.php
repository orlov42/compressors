<?php

namespace Tests\Feature;

use App\Models\PasswordReset;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ForgotPasswordTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test()
    {
        $this->refreshApplication();

        $response = $this
            ->json('POST',
                '/auth/register', [
                    'user_name' => 'Тестирование сброса пароля',
                    'user_email' => 'compressors.unit@yandex.ru',
                    'user_phone' => '+7911111111',
                    'user_password' => '123456',
                    'company_name' => 'Тестирование сброса пароля',
                    'company_inn' => '111111',
                    'company_kpp' => '111111',
                    'company_address_ur' => 'Тестирование сброса пароля',
                    'company_address_fact' => 'Тестирование сброса пароля',
                    'company_bank' => 'Тестирование сброса пароля',
                    'company_index' => '111111'
                ]
            );

        $response->assertStatus(200);
        $this->assertAuthenticated();

        $response = $this->get('auth/logout');
        $response->assertStatus(200);
        $this->assertGuest();

        $this->assertDatabaseHas('users', [
            'user_email' => 'compressors.unit@yandex.ru'
        ]);

        $response = $this->json('POST',
            '/auth/forgot', [
                'user_email' => 'compressors.unit@yandex.ru'
            ]
        );
        $response->assertStatus(200);
        $reset = PasswordReset::where('user_email','compressors.unit@yandex.ru')->first();
        $this->assertNotNull($reset);

        $response = $this->json('POST',
            '/auth/reset', [
                'token' => $reset->token,
                'user_password'=>'1234567'
            ]
        );

        $response->assertStatus(200);

        $this->assertDatabaseMissing('password_resets', [
            'token' => $reset->token
        ]);
        $this->assertAuthenticated();

        $response = $this->get('auth/logout');
        $response->assertStatus(200);
        $this->assertGuest();

        $response = $this->json('POST',
            '/auth/login', [
                'user_email' => 'compressors.unit@yandex.ru',
                'user_password' => '1234567'
            ]
        );

        $response->assertStatus(200);

        $this->assertAuthenticated();
    }
}
